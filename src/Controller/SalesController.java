package Controller;
import Model.*;

import java.util.ArrayList;

public class SalesController {
	SalesCollectionModel salesCollectionModel;
    SalesItemModel salesItemModel;

	public SalesController() {
		salesCollectionModel = SalesCollectionModel.getInstance();
	}

	public void create(double totalPrice, int date, Boolean paid, ArrayList<SalesItemModel> salesItems) {
		UserModel user = LoginModel.getInstance().getUser(); // Currently logged in user.

		// Create a sales, then add to the collection.
		salesCollectionModel.create(user, totalPrice, date, paid, salesItems);
	}

	public ArrayList<SalesModel> getAll() {
		return salesCollectionModel.getAll();
	}

    public ArrayList<SalesItemModel>list(){
        return salesCollectionModel.list();
    }

    public void removeSalesItem(SalesModel s, SalesItemModel si) {
        salesCollectionModel.removeSalesItem(s, si);
    }
}