package Controller;
import java.util.ArrayList;
import Model.*;

public class DiscountController {
    DiscountCollectionModel discountCollectionModel;

    public DiscountController(){
        discountCollectionModel = DiscountCollectionModel.getInstance();
    }
    public void create(String name, int percentage, double priceOff){
        // Create a item, then add to the collection.
        discountCollectionModel.create(name, priceOff, percentage);
    }

    public DiscountModel find(String key){
        return discountCollectionModel.find(key);
    }

    public ArrayList<DiscountModel> findList(String key){
        return discountCollectionModel.getListByKey(key);
    }
    public ArrayList<DiscountModel> list(){
        return discountCollectionModel.getAll();
    }

    public ArrayList<DiscountModel> getAll(){
        return discountCollectionModel.getAll();
    }
}