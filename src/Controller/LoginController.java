package Controller;
import Model.*;

import java.util.ArrayList;

public class LoginController {

	LoginModel loginModel;

	public LoginController() {
		loginModel = LoginModel.getInstance();
	}

	public Boolean login(String username, String password) {
		// Create a login, then add to the collection.
		UserCollectionModel userCollectionModel = UserCollectionModel.getInstance();
		return loginModel.login(username, password, userCollectionModel.getAll());
	}

	public Boolean logout() {
		return loginModel.logout();
	}

	public Boolean getLogin() {
		return loginModel.getLogin();
	}
}