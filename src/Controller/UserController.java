package Controller;
import java.util.ArrayList;
import Model.*;

public class UserController {
	UserCollectionModel userCollectionModel;

	public UserController() {
		userCollectionModel = UserCollectionModel.getInstance();
	}

	public void create(String username, String name, String password, String accountType) {
		// Create a user, then add to the collection.
		userCollectionModel.create(username, name, password, accountType);
	}

	public UserModel find(String key) {
		return userCollectionModel.find(key);
	}

	public ArrayList<UserModel> findList(String key) {
		return userCollectionModel.getListByKey(key);
	}

	public ArrayList<UserModel> getAll() {
		return userCollectionModel.getAll();
	}

}
