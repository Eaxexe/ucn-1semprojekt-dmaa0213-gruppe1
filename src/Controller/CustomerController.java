package Controller;
import Model.*;

import java.util.ArrayList;

public class CustomerController {

	CustomerCollectionModel customerCollectionModel;

	public CustomerController() {
		customerCollectionModel = CustomerCollectionModel.getInstance();
	}

	public void create(String name, String address, String email, String phoneNumber, String userInputCustomerType, String cvr, String discountType) {
		// Create a customer, then add to the collection.
		customerCollectionModel.create(name, address, email, phoneNumber, userInputCustomerType, cvr, discountType);
	}

	public CustomerModel find(String key) {
		return customerCollectionModel.find(key);
	}

    public ArrayList<CustomerModel> findList(String key) {
        return customerCollectionModel.getListByKey(key);
    }

    public ArrayList<CustomerModel> getAll() {
        return customerCollectionModel.getAll();
    }

}