package Controller;
import Model.*;

import java.util.ArrayList;

public class ItemController {
	ItemCollectionModel itemCollectionModel;
    ItemModel itemModel; // WTF IS THIS DOING HERE?
    GroupCollectionModel groupCollectionModel;
    LocationCollectionModel locationCollectionModel;


	public ItemController(){
		itemCollectionModel = ItemCollectionModel.getInstance();
	}

	public void create(String name, String id, double price, int maximumAmount, int minimumAmount, int currentAmount, GroupModel group, ArrayList<ItemLocationModel> location, DealModel dealModel){
		// Create a item, then add to the collection.
		itemCollectionModel.create(name, id, price, maximumAmount, minimumAmount, currentAmount, group, location, dealModel);
	}

	public ItemModel find(String key) {
		return itemCollectionModel.find(key);
	}

    public ArrayList<ItemModel> findList(String key) {
        return itemCollectionModel.getListByKey(key);
    }

    public ArrayList<ItemModel> getAll() {
        return itemCollectionModel.getAll();
    }

    public ArrayList<ItemLocationModel> getAllItemLocations() {
        return itemModel.getAllItemLocations();
    }

    public ArrayList<GroupModel> listGroups(){
        return groupCollectionModel.getAll();
    }
    public ArrayList<LocationModel> listLocations(){
        return locationCollectionModel.getAll();
    }
}