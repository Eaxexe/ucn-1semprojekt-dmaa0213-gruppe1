package Controller;
import Model.*;

import java.util.ArrayList;

public class DealController {

        DealCollectionModel dealCollectionModel;

        public DealController() {
            dealCollectionModel = DealCollectionModel.getInstance();
        }

        public void create(String name, String discountPercentage, String totalPrice) {
            // Create a item, then add to the collection.
            dealCollectionModel.create(name, discountPercentage, totalPrice);
        }

        public DealModel find(String key) {
            return dealCollectionModel.find(key);
        }

    public ArrayList<DealModel> findList(String key) {
        return dealCollectionModel.getListByKey(key);
    }

    public ArrayList<DealModel> getAll() {
        return dealCollectionModel.getAll();
    }
    }

