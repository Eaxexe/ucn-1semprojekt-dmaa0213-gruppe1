package Controller;
import Model.*;

import java.util.ArrayList;

public class LocationController {

	LocationCollectionModel locationCollectionModel;

	public LocationController() {
		locationCollectionModel = LocationCollectionModel.getInstance();
	}

	public void create(String name) {
		// Create a location, then add to the collection.
		locationCollectionModel.create(name);
	}

	public LocationModel find(String key) {
		return locationCollectionModel.find(key);
	}

	public ArrayList<LocationModel> findList(String key) {
		return locationCollectionModel.getListByKey(key);
	}

	public ArrayList<LocationModel> list() {
		return locationCollectionModel.getAll();
	}


}