package Controller;
import Model.*;

import java.util.ArrayList;

public class GroupController {

    GroupCollectionModel groupCollectionModel;

    public GroupController() {
        groupCollectionModel = GroupCollectionModel.getInstance();
    }

    public void create(String name, String id, int antal) {
        // Create a item, then add to the collection.
        groupCollectionModel.create(name, id,antal);
    }

    public GroupModel find(String key) {
        return groupCollectionModel.find(key);
    }

    public ArrayList<GroupModel> findList(String key) {
        return groupCollectionModel.getListByKey(key);
    }

    public ArrayList<GroupModel> getAll() {
        return groupCollectionModel.getAll();
    }
}