package Model;
import java.util.ArrayList;

public class UserCollectionModel {
	private static UserCollectionModel instance;
	private ArrayList<UserModel> userCollection;

	private UserCollectionModel(){
		userCollection = new ArrayList<UserModel>();
	}

	public static UserCollectionModel getInstance(){
		if(instance == null){
			instance = new UserCollectionModel();
		}

		return instance;
	}

	// Find user by string.
	public UserModel find(String key){
		UserModel c = null;
		for(UserModel f : userCollection){
			if(f.getUsername().toLowerCase().contains(key.toLowerCase())){
				c = f;
			} else if(f.getName().toLowerCase().contains(key.toLowerCase())){
				c = f;
			}
		}

		return c;
	}

	// Add user to list
	public void addUser(UserModel c){
		userCollection.add(c);
	}

	// Create user, add to list
	public void create(String username, String name, String password, String accountType){
		UserModel c = new UserModel(username, name, password, accountType);
		addUser(c);
	}

	public ArrayList<UserModel> getAll(){
		return userCollection;
	}

	public ArrayList<UserModel> getListByKey(String key){
		ArrayList<UserModel> returnUsers = new ArrayList<UserModel>();

		for(UserModel f : userCollection){
			if(f.getUsername().toLowerCase().contains(key.toLowerCase())){
				returnUsers.add(f);
			} else if(f.getName().toLowerCase().contains(key.toLowerCase())){ // Error here when finding names.
				returnUsers.add(f);
			}
		}

		return returnUsers;
	}

    public UserModel overwriteUser(UserModel oldCustomer, UserModel newCustomer){
        UserModel c = null;

        for(UserModel f : userCollection){
            if(f == oldCustomer){
                // overwrite
                f.setUsername(newCustomer.getUsername());
                f.setName(newCustomer.getName());
                f.setPassword(newCustomer.getPassword());
                f.setAccountType(newCustomer.getAccountType());
            }
        }

        return c;
    }
}