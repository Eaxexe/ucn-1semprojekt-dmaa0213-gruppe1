package Model;
import java.util.ArrayList;

public class ItemCollectionModel {
	private static ItemCollectionModel instance;
	private ArrayList<ItemModel> itemCollection;

	private ItemCollectionModel() {
		itemCollection = new ArrayList<ItemModel>();
	}

	public static ItemCollectionModel getInstance() {
		if(instance == null){
			instance = new ItemCollectionModel();
		}

		return instance;
	}

	// Find item by string.
	public ItemModel find(String key) {
		ItemModel c = null;
		for(ItemModel f : itemCollection){
			if(f.getName().equals(key)){
				c = f;
			}  else if(f.getId().equals(key)){
				c = f;
			}
		}

		return c;
	}

	// Add item to list
	public void addItem(ItemModel c) {
		itemCollection.add(c);
	}

	// Create item, add to list
	public void create(String name, String id, double price, int maximumAmount, int minimumAmount, int currentAmount, GroupModel group, ArrayList<ItemLocationModel> itemLocation, DealModel dealModel) {
		ItemModel c = new ItemModel(name, id, price, maximumAmount, minimumAmount, currentAmount, group, itemLocation, dealModel);
		addItem(c);
	}

    public ArrayList<ItemModel>getAll() {
       return itemCollection;
    }

    public ArrayList<ItemModel> getListByKey(String key) {
        ArrayList<ItemModel>returnItems = new ArrayList<ItemModel>();

        for(ItemModel f : itemCollection){
            if(f.getName().toLowerCase().contains(key.toLowerCase())){
                returnItems.add(f);
            } else if(f.getName().toLowerCase().contains(key.toLowerCase())){
                returnItems.add(f);
            } else if(f.getGroup().getName().toLowerCase().contains(key.toLowerCase())){
				returnItems.add(f);
			}
        }

        return returnItems;
    }

	// Used when updating an item.
	public ItemModel overwriteItem(ItemModel oldItem, ItemModel newItem){
		ItemModel r = null;

		for(ItemModel i : itemCollection){
			if(i == oldItem){
				// overwrite
				i.setName(newItem.getName());
				i.setGroup(newItem.getGroup());
				i.setId(newItem.getId());
				i.setItemLocations(newItem.getAllItemLocations());
				i.setCurrentAmount(newItem.getCurrentAmount());
				i.setMaximumAmount(newItem.getMaximumAmount());
				i.setMinimumAmount(newItem.getMinimumAmount());
				i.setPrice(newItem.getPrice());
				i.setDealModel(newItem.getDealModel());
				i.setItemLocations(newItem.getAllItemLocations());
				r = i;
			}
		}

		return r;
	}

	// To get from the list. For update purposes
	public ItemModel findbyObject(ItemModel itemModel){
		ItemModel r = null;

		for(ItemModel i : itemCollection){
			if(i == itemModel){
				r = itemModel;
			}
		}

		return r;
	}
}