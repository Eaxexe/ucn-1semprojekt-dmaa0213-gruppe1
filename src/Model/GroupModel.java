package Model;

public class GroupModel {

    private String name;
    private String id;
    private int antal;
    public String getId;

    public GroupModel(String name, String id, int antal) {
        this.name = name;
        this.id = id;
        this.antal = antal;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public int getAntal() {
        return antal;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAntal(int antal) {
        this.antal = antal;
    }
}
