package Model;


public class DealItemsCollection {
    ItemModel itemModel;
    int amount;

    public DealItemsCollection(ItemModel itemModel, int amount) {
        this.itemModel = itemModel;
        this.amount = amount;
    }

    public ItemModel getItemModel() {
        return itemModel;
    }

    public void setItemModel(ItemModel itemModel) {
        this.itemModel = itemModel;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
