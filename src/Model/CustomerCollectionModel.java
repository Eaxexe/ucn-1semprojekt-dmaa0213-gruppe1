package Model;
import java.util.ArrayList;

public class CustomerCollectionModel {
	private static CustomerCollectionModel instance;
	private ArrayList<CustomerModel> customerCollection;

	private CustomerCollectionModel(){
		customerCollection = new ArrayList<CustomerModel>();
	}

	public static CustomerCollectionModel getInstance(){
		if(instance == null){
			instance = new CustomerCollectionModel();
		}

		return instance;
	}

	// Find customer by string.
	public CustomerModel find(String key){
		CustomerModel c = null;
		for(CustomerModel f : customerCollection){
			if(f.getName().toLowerCase().equals(key.toLowerCase())){
				c = f;
			}  else if(f.getEmail().toLowerCase().equals(key.toLowerCase())){
				c = f;
			}
		}

		return c;
	}

	// Used when updating a customer.
	public CustomerModel overwriteCustomer(CustomerModel oldCustomer, CustomerModel newCustomer){
		CustomerModel c = null;

		for(CustomerModel f : customerCollection){
			if(f == oldCustomer){
				// overwrite
				f.setName(newCustomer.getName());
				f.setCvr(newCustomer.getCvr());
				f.setEmail(newCustomer.getEmail());
				f.setAddress(newCustomer.getAddress());
				f.setPhoneNumber(newCustomer.getPhoneNumber());
				f.setCustomerType(newCustomer.getCustomerType());
				f.setDiscountType(newCustomer.getDiscountType());
			}
		}

		return c;
	}

	// Add customer to list
	public void addCustomer(CustomerModel c){
		customerCollection.add(c);
	}

	// Create customer, add to list
	public void create(String name, String address, String email, String phoneNumber, String userInputCustomerType, String cvr, String discountType){
		CustomerModel c = new CustomerModel(name, address, email, phoneNumber, userInputCustomerType, cvr, discountType);
		addCustomer(c);
	}

    public ArrayList<CustomerModel>getAll(){
        return customerCollection;
    }

    public ArrayList<CustomerModel> getListByKey(String key){
        ArrayList<CustomerModel>returnCustomerModel = new ArrayList<CustomerModel>();

        for(CustomerModel f : customerCollection){
            if(f.getName().toLowerCase().contains(key.toLowerCase())){
                returnCustomerModel.add(f);
            } else if(f.getName().toLowerCase().contains(key.toLowerCase())){
                returnCustomerModel.add(f);
            }
        }

        return returnCustomerModel;
    }
}