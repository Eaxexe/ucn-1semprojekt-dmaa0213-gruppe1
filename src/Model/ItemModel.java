package Model;
import java.util.ArrayList;

public class ItemModel {
	private String name;
    private String id;
	private double price;
    private int maximumAmount;
    private int minimumAmount;
    private int currentAmount;
	private GroupModel group;
    private ArrayList<ItemLocationModel> itemLocations;
	private DealModel dealModel;

    public ItemModel(String name, String id, double price, int maximumAmount, int minimumAmount, int currentAmount, GroupModel group, ArrayList<ItemLocationModel> location, DealModel dealModel) {
        this.name = name;
        this.id = id;
        this.price = price;
        this.maximumAmount = maximumAmount;
        this.minimumAmount = minimumAmount;
        this.currentAmount = currentAmount;
        this.group = group;

		if(location == null){
			this.itemLocations = new ArrayList<ItemLocationModel>();
		}
		this.dealModel = dealModel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setMaximumAmount(int maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public void setMinimumAmount(int minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public void setCurrentAmount(int currentAmount) {
        this.currentAmount = currentAmount;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public int getMaximumAmount() {
        return maximumAmount;
    }

    public int getMinimumAmount() {
        return minimumAmount;
    }

    public int getCurrentAmount() {
        return currentAmount;
    }

    public void addItemLocation(ItemLocationModel i){
		itemLocations.add(i);
    }

    public void removeItemLocation(ItemLocationModel i){
        itemLocations.remove(i);
    }

	public GroupModel getGroup() {
		return group;
	}

	public void setGroup(GroupModel group) {
		this.group = group;
	}

	public ArrayList<ItemLocationModel> getLocations() {
		return itemLocations;
	}

	public void setItemLocations(ArrayList<ItemLocationModel> itemLocations) {
		this.itemLocations = itemLocations;
	}

	public ItemLocationModel find(String key){
        ItemLocationModel c = null;
        for(ItemLocationModel f : itemLocations){
            if(f.getLocation().equals(key)){
                c = f;
            }  else if(f.getShelf().equals(key)){
                c = f;
            }
        }
        return c;
    }

    public ArrayList<ItemLocationModel> getAllItemLocations(){
        return itemLocations;
    }

	public DealModel getDealModel() {
		return dealModel;
	}

	public void setDealModel(DealModel dealModel) {
		this.dealModel = dealModel;
	}

	public void removeLocationByName(String name){
		for(ItemLocationModel i : itemLocations){
			if(i.getLocation().getName().equals(name)){
				itemLocations.remove(i);
			}
		}
	}
}