package Model;
import java.util.ArrayList;

public class DealCollectionModel {
    private static DealCollectionModel instance;
    private ArrayList<DealModel> dealCollection;

    private DealCollectionModel(){
        dealCollection = new ArrayList<DealModel>();
    }

    public static DealCollectionModel getInstance(){
        if(instance == null){
            instance = new DealCollectionModel();
        }

        return instance;
    }

    // Find deal by string.
    public DealModel find(String key){
        DealModel c = null;
        for(DealModel f : dealCollection){
            if(f.getName().equals(key)){
                c = f;
            }  else if(f.getDiscountPercentage().equals(key)){
                c = f;
            }
        }

        return c;
    }

    // Add customer to list
    public void addDeal(DealModel c){
        dealCollection.add(c);
    }


    public ArrayList<DealModel>getAll(){
        return dealCollection;
    }

    // Create Deal, add to list
    public void create(String name, String totalPrice, String discountPercentage){
        DealModel c = new DealModel(name, totalPrice, discountPercentage);
        addDeal(c);
    }
    public ArrayList<DealModel> getListByKey(String key){
        ArrayList<DealModel>returnDeal = new ArrayList<DealModel>();

        for(DealModel f : dealCollection){
            if(f.getName().toLowerCase().contains(key.toLowerCase())){
                returnDeal.add(f);
            } else if(f.getName().toLowerCase().contains(key.toLowerCase())){
                returnDeal.add(f);
            }
        }

        return returnDeal;
    }

    public DealModel overwriteDeal(DealModel oldDeal, DealModel newDeal){
        DealModel c = null;

        for(DealModel f : dealCollection){
            if(f == oldDeal){
                // overwrite
                f.setName(newDeal.getName());
                f.setDiscountPercentage(newDeal.getDiscountPercentage());
                f.setTotalPrice(newDeal.getTotalPrice());
                            }
        }

        return c;
    }
}