package Model;

import java.util.ArrayList;

public class DiscountCollectionModel {

        private static DiscountCollectionModel instance;
        private ArrayList<DiscountModel> discountCollection;

        private DiscountCollectionModel(){
            discountCollection = new ArrayList<DiscountModel>();
        }

        public static DiscountCollectionModel getInstance(){
            if(instance == null){
                instance = new DiscountCollectionModel();
            }

            return instance;
        }

        // Find deal by string.
        public DiscountModel find(String key){
            DiscountModel c = null;
            for(DiscountModel f : discountCollection){
                if(f.getName().equals(key)){
                    c = f;
                }  else if(f.getName().equals(key)){
                    c = f;
                }
            }

            return c;
        }

        // Add customer to list
        public void addDiscount(DiscountModel c){
            discountCollection.add(c);
        }


        public ArrayList<DiscountModel>getAll(){
            return discountCollection;
        }

        // Create Discount, add to list
        public void create(String name, double priceOff, int percentage){
            DiscountModel c = new DiscountModel(name,percentage,priceOff);
            addDiscount(c);
        }
        public ArrayList<DiscountModel> getListByKey(String key){
            ArrayList<DiscountModel>returnDiscount = new ArrayList<DiscountModel>();

            for(DiscountModel f : discountCollection){
                if(f.getName().toLowerCase().contains(key.toLowerCase())){
                    returnDiscount.add(f);
                } else if(f.getName().toLowerCase().contains(key.toLowerCase())){
                    returnDiscount.add(f);
                }
            }

            return returnDiscount;
        }
    }
