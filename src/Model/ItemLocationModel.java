package Model;

public class ItemLocationModel {
	private LocationModel location;
    private String shelf;
    private int amount;

	public ItemLocationModel(LocationModel location, String shelf, int amount) {
		this.location = location;
		this.shelf = shelf;
		this.amount = amount;
	}

	public LocationModel getLocation() {
		return location;
	}

	public void setLocation(LocationModel location) {
		this.location = location;
	}

	public String getShelf() {
		return shelf;
	}

	public void setShelf(String shelf) {
		this.shelf = shelf;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
