package Model;

import java.util.ArrayList;

public class SalesModel {
	private UserModel user;
	double totalPrice;
	int date; // Unix time.
	private Boolean isPaid;
	private ArrayList<SalesItemModel> salesItems;

	public SalesModel(UserModel user, double totalPrice, int date, Boolean paid, ArrayList<SalesItemModel> salesItems) {
		this.user = user;
		this.totalPrice = totalPrice;
		this.date = date;
		this.isPaid = paid;
		this.salesItems = salesItems;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public Boolean getPaid() {
		return isPaid;
	}

	public void setPaid(Boolean paid) {
		isPaid = paid;
	}

	public ArrayList<SalesItemModel> getSalesItems() {
		return salesItems;
	}

	public void setSalesItems(ArrayList<SalesItemModel> salesItems) {
		this.salesItems = salesItems;
	}

    public void removeSalesItem(SalesItemModel item) {
        this.salesItems.remove(item);
    }
}
