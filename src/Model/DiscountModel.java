package Model;


public class DiscountModel {
    private String name, type;
    private int percentage;
    private double priceOff;
    ItemModel item;

    public DiscountModel(String name, int percentage, double priceOff) {
        this.name = name;
        this.percentage = percentage;
        this.priceOff = priceOff;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }


    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public double getPriceOff() {
        return priceOff;
    }

    public void setPriceOff(double priceOff) {
        this.priceOff = priceOff;
    }

    public ItemModel getItem() {
        return item;
    }

    public void setItem(ItemModel item) {
        this.item = item;
    }
}
