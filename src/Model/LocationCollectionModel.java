package Model;
import java.util.ArrayList;

public class LocationCollectionModel {
	private static LocationCollectionModel instance;
	private ArrayList<LocationModel> locationCollection;

	private LocationCollectionModel(){
		locationCollection = new ArrayList<LocationModel>();
	}

	public static LocationCollectionModel getInstance(){
		if(instance == null){
			instance = new LocationCollectionModel();
		}

		return instance;
	}

	// Find location by string.
	public LocationModel find(String key){
		LocationModel l = null;
		for(LocationModel f : locationCollection){
			if(f.getName().toLowerCase().equals(key.toLowerCase())){
				l = f;
			}
		}

		return l;
	}

	// Add location to list
	public void addLocation(LocationModel c){
		locationCollection.add(c);
	}

	// Create location, add to list
	public void create(String name){
		LocationModel c = new LocationModel(name);
		addLocation(c);
	}

	public ArrayList<LocationModel> getAll(){
		return locationCollection;
	}

	public ArrayList<LocationModel> getListByKey(String key){
		ArrayList<LocationModel>returnLocations = new ArrayList<LocationModel>();

		for(LocationModel f : locationCollection){
			if(f.getName().toLowerCase().contains(key.toLowerCase())){
				returnLocations.add(f);
			}
		}

		return returnLocations;
	}

    public LocationModel overwriteLocation(LocationModel oldLocation, LocationModel newLocation){
        LocationModel c = null;

        for(LocationModel f : locationCollection){
            if(f == oldLocation){
                // overwrite
                f.setName(newLocation.getName());
            }
        }
        return c;
    }

}