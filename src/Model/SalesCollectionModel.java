package Model;
import java.util.ArrayList;

public class SalesCollectionModel {
	private static SalesCollectionModel instance;
	private ArrayList<SalesModel> salesCollection;
    private ArrayList<SalesItemModel>salesItems;

	private SalesCollectionModel(){
		salesCollection = new ArrayList<SalesModel>();
	}

	public static SalesCollectionModel getInstance(){
		if(instance == null){
			instance = new SalesCollectionModel();
		}

		return instance;
	}

	// Add a sale to list
	public void addSales(SalesModel c){
		salesCollection.add(c);
	}

	// Create a sale, add to list
	public void create(UserModel user, double totalPrice, int date, Boolean paid, ArrayList<SalesItemModel> salesItems){
		SalesModel c = new SalesModel(user, totalPrice, date, paid, salesItems);
		addSales(c);
	}

	public ArrayList<SalesModel> getAll(){
		return salesCollection;
	}

    public ArrayList<SalesItemModel>list(){
        return salesItems;
    }
    public void removeSalesItem(SalesModel s, SalesItemModel si) {
        s.removeSalesItem(si);
    }
}