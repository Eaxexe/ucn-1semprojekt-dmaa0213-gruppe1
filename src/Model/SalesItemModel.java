package Model;

public class SalesItemModel {
	ItemModel item;
	double salesPrice;
	int amount;

	public SalesItemModel(ItemModel item, double salesPrice, int amount) {
		this.item = item;
		this.salesPrice = salesPrice;
		this.amount = amount;
	}

	public ItemModel getItem() {
		return item;
	}

	public void setItem(ItemModel item) {
		this.item = item;
	}

	public double getSalesPrice() {
		return salesPrice;
	}

	public void setSalesPrice(double salesPrice) {
		this.salesPrice = salesPrice;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}