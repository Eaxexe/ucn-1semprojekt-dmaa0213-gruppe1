package Model;


public class DealModel {
    private String name, discountPercentage;
    private String totalPrice;

    public DealModel(String name, String discountPercentage, String totalPrice) {
        this.name = name;
        this.totalPrice = totalPrice;
        this.discountPercentage = discountPercentage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }
}

