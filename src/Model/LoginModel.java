package Model;
import java.util.ArrayList;

public class LoginModel {
	private static LoginModel instance;
	private UserModel user;
	private Boolean loginStatus;

	public static LoginModel getInstance(){
		if(instance == null){
			instance = new LoginModel();
		}

		return instance;
	}

	public Boolean login(String username, String password, ArrayList<UserModel> userList){
		for(UserModel f : userList){
			if(f.getUsername().equals(username) && f.getPassword().equals(password)){
				this.setLoginStatus(true);
				this.user = f;
				return true;
			}
		}

		return false;
	}

	public Boolean logout(){
		this.setLoginStatus(false);
		this.user = null;
		return true;
	}

	public Boolean getLogin(){
		return this.getLoginStatus();
	}

	public static void setInstance(LoginModel instance) {
		LoginModel.instance = instance;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public Boolean getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(Boolean loginStatus) {
		this.loginStatus = loginStatus;
	}
}