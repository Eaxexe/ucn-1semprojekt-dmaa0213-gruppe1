package Model;
import java.util.ArrayList;

public class GroupCollectionModel {
    private static GroupCollectionModel instance;
    private ArrayList<GroupModel> groupCollection;

    private GroupCollectionModel(){
        groupCollection = new ArrayList<GroupModel>();
    }

    public static GroupCollectionModel getInstance(){
        if(instance == null){
            instance = new GroupCollectionModel();
        }

        return instance;
    }

    // Find item by string.
    public GroupModel find(String key){
        GroupModel c = null;
        for(GroupModel f : groupCollection){
            if(f.getName().toLowerCase().equals(key)){
                c = f;
            }  else if(f.getId().equals(key)){
                c = f;
            }
        }

        return c;
    }

    // Add item to list
    public void addItem(GroupModel c){
        groupCollection.add(c);
    }

    // Create item, add to list
    public void create(String name, String id, int antal){
        GroupModel c = new GroupModel(name, id, antal);
        addItem(c);
    }

	// Get all groups.
	public ArrayList<GroupModel> getGroupCollection(){
		return groupCollection;
	}


    public ArrayList<GroupModel>getAll(){
        return groupCollection;
    }

    public ArrayList<GroupModel> getListByKey(String key){
        ArrayList<GroupModel>returnGroup = new ArrayList<GroupModel>();

        for(GroupModel f : groupCollection){
            if(f.getName().toLowerCase().contains(key.toLowerCase())){
                returnGroup.add(f);
            } else if(f.getName().toLowerCase().contains(key.toLowerCase())){
                returnGroup.add(f);
            }
        }

        return returnGroup;
    }

    // Used when updating a customer.
    public GroupModel overwriteGroups(GroupModel oldGroup, GroupModel newGroup){
        GroupModel c = null;

        for(GroupModel f : groupCollection){
            if(f == oldGroup){
                // overwrite
                f.setName(newGroup.getName());
                f.setId(newGroup.getId());
                f.setAntal(newGroup.getAntal());
            }
        }

        return c;
    }
}