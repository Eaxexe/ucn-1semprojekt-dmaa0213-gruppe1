package GUIView;

import Model.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class SalesView extends JPanel {
	private JPanel salesPanel;
	private JScrollPane salesScroll;
	private JTable salesTable;

	private JButton paymentButton;
	private JButton clearButton;
	private JButton addItemButton;
	private JButton selectCustomer;
    private JLabel Totalvalue;
    ArrayList<SalesItemModel> salesItems;
	Map<Integer, SalesItemModel> sales;
	DefaultTableModel defModel;
	String[] columnNames = {"Navn", "Antal", "Orginal Pris","Pris efter rabat", "Total", "Ændr", "Slet"};
	Object[][] data = {};


	public SalesView(){
		// Add a sale.
		salesItems = new ArrayList<SalesItemModel>();
		salesItems.add(new SalesItemModel(ItemCollectionModel.getInstance().find("Dyknagler 10stk"), 150, 200));
		salesItems.add(new SalesItemModel(ItemCollectionModel.getInstance().find("Hampereb 5m"), 100, 100));
        salesItems.add(new SalesItemModel(ItemCollectionModel.getInstance().find("Steppeløber 10K"), 100, 150));
		SalesCollectionModel.getInstance().create(LoginModel.getInstance().getUser(), 100, 100, true, salesItems);
		sales = new HashMap<Integer, SalesItemModel>();
		int n = 1;
		for(SalesItemModel s : salesItems){
			// Make a list of items that the user can select.                       w
			sales.put(n, s);
			n++;
		}

		defModel = new DefaultTableModel(data, columnNames);
		salesTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
		salesTable.setModel(defModel);

		// Add the data.

		for(int i = 1; i < n; i++){
			SalesItemModel x = sales.get(i);
			defModel.addRow(new Object[]{x.getItem().getName(), x.getAmount(), x.getSalesPrice(),null , (x.getAmount() * x.getSalesPrice()), "Ændr", "Slet"});
		}

		// Delete action.
		Action delete = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable)e.getSource();
				Object[] options = {"Ja", "Nej"};
				JDialog frame = new JDialog();
				int n = JOptionPane.showOptionDialog(frame,
						"Er du sikker på at du vil fjerne denne vare fra kurven?",
						"Fjern fra kurven",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[0]);

				if(n == JOptionPane.YES_OPTION){
					int modelRow = Integer.valueOf( e.getActionCommand() );
					salesItems.remove(sales.get(modelRow)); // Remove the item itself.
					((DefaultTableModel)table.getModel()).removeRow(modelRow);
				}
			}
		};

		// Edit action.
		Action edit = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				int modelRow = Integer.valueOf( e.getActionCommand() );
				// Let's add all the junk we want to this dialog!
				String amount = JOptionPane.showInputDialog(null, "Indtast antal: ", "", 1);
				if(amount != null) {
					//Update the amount on the SalesItemModel.
					sales.get(modelRow + 1).setAmount(Integer.parseInt(amount));
					// Set the amount in the row.
					defModel.setValueAt(amount, modelRow, 1);
					// Set the new total in the row.
					int newAmount = Integer.parseInt(amount);
					double price = sales.get(modelRow+1).getItem().getPrice();
					double newtotal =   newAmount * price;
					defModel.setValueAt(newtotal, modelRow, 4);

					// Should also update the actual total
					defModel.fireTableDataChanged();
			 	}
			}
		};

		TableColumn editColumn = salesTable.getColumn("Ændr");
		editColumn.setCellRenderer(new ButtonColumn(salesTable, edit, 5));
		TableColumn deleteColumn = salesTable.getColumn("Slet");
		deleteColumn.setCellRenderer(new ButtonColumn(salesTable, delete, 6)); // IF you are getting errors, these two are probably it.

		// Clear the sales items.

		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] options = {"Ja", "Nej"};
				JDialog frame = new JDialog();
				int n = JOptionPane.showOptionDialog(frame,
						"Er du sikker på at du vil fjerne ALLE varer i kurven?",
						"Ryd kurv",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[0]);

				if(n == JOptionPane.YES_OPTION){
					// Clear the map
					sales.clear();
					// Set new model.
					defModel = new DefaultTableModel(data, columnNames);
					salesTable.setModel(defModel);
				}
			}
		});
	}

	public JPanel getSalesPanel() {
		return salesPanel;
	}
}

