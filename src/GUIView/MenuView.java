package GUIView;

import Model.LoginModel;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.*;

public class MenuView extends JFrame {
	private JPanel contentPane;
	private JTabbedPane menuTabs;
	private JButton logUdButton;
	private JLabel userStatus;

	public MenuView() {
		setContentPane(contentPane);
		userStatus.setText("Logget ind som: " + LoginModel.getInstance().getUser().getName() + " - " + LoginModel.getInstance().getUser().getAccountType());
		//setModal(true);

		menu();
	}

	public void menu() {
		Dimension screenResolution = Toolkit.getDefaultToolkit().getScreenSize();

		// We are adding our tabs manually. Why? Because I say so!
		JPanel sales = new SalesView().getSalesPanel();
		menuTabs.addTab("Salg", sales);

		JPanel items = new ItemView().getItemPanel();
		menuTabs.addTab("Varer", items);

		JPanel groups = new GroupView().getGroupPanel();
		menuTabs.addTab("Varegrupper", groups);

		JPanel deals = new DealView().getDealPanels();
		menuTabs.addTab("Tilbud", deals);

		JPanel customers = new CustomerView().getCustomerPanel();
		menuTabs.addTab("Kunder", customers);

		JPanel administration = new AdministratorView().getAdministrationPanel();
		menuTabs.addTab("Administration", administration);

		menuTabs.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                //System.out.println("Tab=" + menuTabs.getSelectedIndex());
            }
        });

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.setTitle("Sandy Ambien Nights - V0.0.1");
		this.pack();
		this.setLocation(0, 0);
		this.setSize((int)screenResolution.getWidth(), (int)screenResolution.getHeight());
		this.setVisible(true);
		//System.exit(0);
	}

	public void tabChange(){

	}
}
