package GUIView;

import Model.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class ItemView extends JPanel {
	private JPanel itemPanel;
	private JScrollPane itemScroll;
	private JTable itemTable;

	private JButton createButton;
	private JButton clearButton;
	private JTextField searchTextField;
	private JButton alleButton;
	ArrayList<ItemModel> itemList;
	Map<Integer, ItemModel> items;
	DefaultTableModel defModel;
	String[] columnNames = { "Navn", "Varenummer", "Varegruppe", "Lokation", "Nuværende Antal", "Maks Antal", "Pris", "Vis", "Tilføj lokation", "Fjern lokation", "Ændr", "Fjern" };
	Object[][] data = {};
	int mapLength = 0;

	public ItemView() {
		getAllItems();

		searchTextField.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if(searchTextField.getText().equals("Søg")){
					searchTextField.setText("");
				}
			}
		});

		// Create button.
		createButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				ItemModel cc = itemPopup(null);
				if(cc != null){
					ItemCollectionModel.getInstance().addItem(cc); // Added to the "db"
					items.put(mapLength + 1, cc);
					mapLength++;
					getAllItems(); // Reload the table.
				}
			}
		});

		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<ItemModel> foundItems = ItemCollectionModel.getInstance().getListByKey(searchTextField.getText());
				if(foundItems == null){
					JOptionPane.showMessageDialog(null, "Ingen varer fundet, prøv venligst igen!");
				} else {
					findItemList(foundItems);
				}
			}
		});

		alleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getAllItems();
			}
		});
	}

	public JPanel getItemPanel() {
		return itemPanel;
	}

	public void getAllItems(){
		items = new HashMap<Integer, ItemModel>();
		itemList = ItemCollectionModel.getInstance().getAll();

		mapLength = 0;
		for(ItemModel s : itemList){
			// Make a list of items that the user can select.
			items.put(mapLength, s);
			mapLength++;
		}

		defModel = new DefaultTableModel(data, columnNames);
		itemTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
		itemTable.setModel(defModel);

		// Add the items to the table.
		for(int i = 0; i < mapLength; i++){
			ItemModel x = items.get(i);
			defModel.addRow(new Object[]{x.getName(), x.getId(), x.getGroup().getName(), "Ukendt", x.getCurrentAmount(), x.getMaximumAmount(), x.getPrice(), "Vis", "Tilføj Lokation", "Fjern Lokation", "Ændr", "Fjern"});
		}

		prepareButtons();
	}

	public void findItemList(ArrayList<ItemModel> itemList){
		items = new HashMap<Integer, ItemModel>();

		mapLength = 0;
		for(ItemModel s : itemList){
			// Make a list of items that the user can select.
			items.put(mapLength, s);
			mapLength++;
		}

		defModel = new DefaultTableModel(data, columnNames);
		itemTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
		itemTable.setModel(defModel);

		// Add the items to the table.
		for(int i = 0; i < mapLength; i++){
			ItemModel x = items.get(i);
			defModel.addRow(new Object[]{x.getName(), x.getId(), x.getGroup().getName(), "Ukendt", x.getCurrentAmount(), x.getMaximumAmount(), x.getPrice(), "Vis", "Tilføj Lokation", "Fjern Lokation", "Ændr", "Fjern"});
		}

		prepareButtons();
	}

	public void prepareButtons(){


		// Show action.
		Action show = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				int modelRow = Integer.valueOf( e.getActionCommand() );
				ItemModel item = items.get(modelRow);

				String message = "";
				message += "Navn: "+item.getName()+"\n";
				message += "Varenummer: "+item.getId()+"\n";
				message += "Varegruppe: "+item.getGroup().getName()+"\n";
				message += "Pris: "+item.getPrice()+"\n";
				message += "Maks antal: "+item.getMaximumAmount()+"\n";
				message += "Minimum antal: "+item.getMinimumAmount()+"\n";
				message += "Nuværende antal: "+item.getCurrentAmount()+"\n";
				if(item.getDealModel() != null){
					message += "Tilbud: "+item.getDealModel().getName()+"\n";
					if(!item.getDealModel().getDiscountPercentage().equals("")){
						message += "Procent Tilbud: "+item.getDealModel().getDiscountPercentage()+"\n";
					} else if(!item.getDealModel().getTotalPrice().equals("")){
						message += "Ny pris: "+item.getDealModel().getTotalPrice()+"\n";
					}
					message += "_____________";
				}

				if(item.getAllItemLocations().size() > 0){
					message += "Kan findes på:\n";
					for(ItemLocationModel i : item.getAllItemLocations()){
						message += "\tLager: "+i.getLocation().getName()+"\n";
						message += "\tHylde: "+i.getShelf()+"\n";
						message += "\tAntal: "+i.getAmount()+"\n\n";
					}
				}

				JOptionPane.showMessageDialog(getParent(),
						message,
						item.getName(),
						JOptionPane.PLAIN_MESSAGE);

			}
		};

		// Edit action.
		Action edit = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				int modelRow = Integer.valueOf( e.getActionCommand() );
				ItemModel x = items.get(modelRow);
				if(x != null) {
					ItemModel y = itemPopup(x);

					if(y != null){ // Pressed OK!
						items.put(modelRow, y); // DOES THIS UPDATE THE ARRAYLIST???  - no
						ItemCollectionModel.getInstance().overwriteItem(x, y);

						// Update the line.
						defModel.setValueAt(y.getName(), modelRow, 0);
						defModel.setValueAt(y.getId(), modelRow, 1);
						defModel.setValueAt(y.getGroup().getName(), modelRow, 2);
						defModel.setValueAt("Ukendt", modelRow, 3);
						defModel.setValueAt(y.getCurrentAmount(), modelRow, 4);
						defModel.setValueAt(y.getMaximumAmount(), modelRow, 5);
						defModel.setValueAt(y.getPrice(), modelRow, 6);
						defModel.fireTableDataChanged();
					}
				}
			}
		};

		// Add Location action.
		Action addLocation = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				JComboBox locationSelection = new JComboBox();
				for(LocationModel l : LocationCollectionModel.getInstance().getAll()){
					locationSelection.addItem(l.getName());
				}

				JTextField shelfField = new JTextField();
				JTextField amountField = new JTextField();

				Object[] message = {
						"Lager: ", locationSelection,
						"Hyldeplads: ", shelfField,
						"Mængde: ", amountField,

				};

				int option = JOptionPane.showConfirmDialog(getParent(), message, "Tilføj lokation", JOptionPane.OK_CANCEL_OPTION);

				if(option == JOptionPane.OK_OPTION) {
					int modelRow = Integer.valueOf( e.getActionCommand() );
					String shelf = shelfField.getText();
					String amount = amountField.getText();
					String location = String.valueOf(locationSelection.getSelectedItem());
					ItemLocationModel i = new ItemLocationModel(LocationCollectionModel.getInstance().find(location), shelf, Integer.parseInt(amount));
					// Add the locationModel to the array.
					//items.get(modelRow).addItemLocation(i); // It has been added to the location array, but I'm not sure that it has been updated in the instance.
					ItemCollectionModel.getInstance().findbyObject(items.get(modelRow)).addItemLocation(i); // Update the original list.
				}
			}
		};

		// Remove Location action.
		Action removeLocation = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				int modelRow = Integer.valueOf( e.getActionCommand() );

				ArrayList<ItemLocationModel> locations = items.get(modelRow).getAllItemLocations();

				JComboBox locationSelection = new JComboBox();
				for(ItemLocationModel l : locations){
					locationSelection.addItem(l.getLocation().getName());
				}

				Object[] message = {
						"Vælg lager: ", locationSelection
				};

				int option = JOptionPane.showConfirmDialog(getParent(), message, "Fjern lokation", JOptionPane.OK_CANCEL_OPTION);

				if(option == JOptionPane.OK_OPTION) {
					String location = String.valueOf(locationSelection.getSelectedItem());

					ItemCollectionModel.getInstance().find(items.get(modelRow).getName()).removeLocationByName(location);
					//items.get(modelRow).getAllItemLocations().remove(modelRow);
					// Remove the locationModel to the array.
					//items.get(modelRow).removeItemLocation(i); // It has been added to the location array, but I'm not sure that it has been updated in the instance.
					//ItemCollectionModel.getInstance().findbyObject(items.get(modelRow)).addItemLocation(i); // Update the original list.
					// We also have to update the original array list, but fuck that for now.
				}
			}
		};

		// Delete action.
		Action delete = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable)e.getSource();
				Object[] options = {"Ja", "Nej"};
				JDialog frame = new JDialog();
				int n = JOptionPane.showOptionDialog(frame,
						"Er du sikker på at du vil fjerne denne vare?",
						"Fjern vare",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[0]);

				if(n == JOptionPane.YES_OPTION){
					int modelRow = Integer.valueOf( e.getActionCommand() );
					itemList.remove(items.get(modelRow)); // Remove the item itself.
					// This should actually be done on the CONTROLLER! Or what? No it shouldn't.
					((DefaultTableModel)table.getModel()).removeRow(modelRow);
				}
			}
		};

		TableColumn showColumn = itemTable.getColumn("Vis");
		showColumn.setCellRenderer(new ButtonColumn(itemTable, show, 7));
		showColumn.setMaxWidth(70);
		TableColumn addLocationColumn = itemTable.getColumn("Tilføj lokation");
		addLocationColumn.setCellRenderer(new ButtonColumn(itemTable, addLocation, 8));
		addLocationColumn.setMaxWidth(120);
		TableColumn removeLocationColumn = itemTable.getColumn("Fjern lokation");
		removeLocationColumn.setCellRenderer(new ButtonColumn(itemTable, removeLocation, 9));
		removeLocationColumn.setMaxWidth(120);
		TableColumn editColumn = itemTable.getColumn("Ændr");
		editColumn.setCellRenderer(new ButtonColumn(itemTable, edit, 10));
		editColumn.setMaxWidth(70);
		TableColumn deleteColumn = itemTable.getColumn("Fjern");
		deleteColumn.setCellRenderer(new ButtonColumn(itemTable, delete, 11)); // IF you are getting errors, these two are probably it.
		deleteColumn.setMaxWidth(70);
	}


	public ItemModel itemPopup(ItemModel existingData){
		ItemModel returnModel = null;

		JComboBox itemGroupSelection = new JComboBox();
		for(GroupModel g : GroupCollectionModel.getInstance().getAll()){
			itemGroupSelection.addItem(g.getName());
		}

		JComboBox dealSelection = new JComboBox();
		for(DealModel d : DealCollectionModel.getInstance().getAll()){
			dealSelection.addItem(d.getName());
		}

		// Set everything to null.
		if(existingData == null){
			existingData = new ItemModel("", "", 0, 0, 0, 0, null, null, null);
			itemGroupSelection.setSelectedIndex(0);
		} else {
			// Find out how to handle this one.
			if(existingData.getGroup() != null){
				itemGroupSelection.setSelectedItem(existingData.getGroup().getName());
			}
			if(existingData.getDealModel() != null){
				dealSelection.setSelectedItem(existingData.getDealModel().getName());
			}
		}

		JTextField nameField = new JTextField(existingData.getName());
		JTextField idField = new JTextField(existingData.getId());
		JTextField currentField = new JTextField(Integer.toString(existingData.getCurrentAmount()));
		JTextField maximumField = new JTextField(Integer.toString(existingData.getMaximumAmount()));
		JTextField minimumField = new JTextField(Integer.toString(existingData.getMinimumAmount()));
		JTextField priceField = new JTextField(Double.toString(existingData.getPrice())); // Is double.

		Object[] message = {
				"Navn: ", nameField,
				"Varenummer: ", idField,
				"Varegruppe: ", itemGroupSelection,
				"Nuværende antal: ", currentField,
				"Minimum antal: ", minimumField,
				"Maks antal: ", maximumField,
				"Pris: ", priceField,
				"Tilbudstype: ", dealSelection
		};

		int option = JOptionPane.showConfirmDialog(getParent(), message, "Enter all your values", JOptionPane.OK_CANCEL_OPTION);

		if(option == JOptionPane.OK_OPTION) {
			String name = nameField.getText();
			String id = idField.getText();
			String current = currentField.getText();
			String maximum = maximumField.getText();
			String minimum = minimumField.getText();
			String price = priceField.getText();
			String itemGroupName = String.valueOf(itemGroupSelection.getSelectedItem());
			String dealName = String.valueOf(dealSelection.getSelectedItem());
			GroupModel group = GroupCollectionModel.getInstance().find(itemGroupName.toLowerCase());
			DealModel dealModel = DealCollectionModel.getInstance().find(dealName.toLowerCase());

			// We have everything, it does need to be validated, but fuck that for now. - Create a Item
			returnModel = new ItemModel(name, id, Double.parseDouble(price), Integer.parseInt(minimum), Integer.parseInt(maximum), Integer.parseInt(current), group, null, dealModel);
		}

		return returnModel;
	}
}
