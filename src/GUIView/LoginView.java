package GUIView;
import Controller.*;
import Model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class LoginView extends JFrame {
	private JPanel contentPane;
	private JButton logInButton;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JButton exitButton;
	private JButton buttonOK;
	private JButton buttonCancel;

	public LoginView() {
		ItemController itemController = new ItemController();
        UserCollectionModel.getInstance().create("alekon", "Alexander", "willy", "Administrator");
		UserCollectionModel.getInstance().create("eax", "Eax Exe", "erhot", "Administrator");
        UserCollectionModel.getInstance().create("kitzhi","Rikke","password","Bruger");

        LocationCollectionModel.getInstance().create("Trælast");
        LocationCollectionModel.getInstance().create("Byggemarked");

		GroupCollectionModel.getInstance().create("Things", "THNGS", 0);
		GroupCollectionModel.getInstance().create("Other Things", "OTHRTHNGS", 0);
		// We have two groups, add them to two ItemGroups.

		itemController.create("Dyknagler 10stk", "DKNGLR", 150, 500, 50, 150, GroupCollectionModel.getInstance().find("things"), null, null);
		itemController.create("Hampereb 5m", "HMPRB", 200, 50, 10, 15, GroupCollectionModel.getInstance().find("things"), null, null);
		itemController.create("Steppeløber 10K", "STPLB", 125, 25, 50, 20, GroupCollectionModel.getInstance().find("other things"), null, null);
		CustomerCollectionModel.getInstance().create("Eax", "Odinsgade 109, St 1", "eax@eax.dk", "30827331", "Erhverv", "34276749", "mor");
		CustomerCollectionModel.getInstance().create("Alexander", "Runddyssen 80", "muleborn@gmail.com", "12345678", "Privat", "0", "nej");

		setContentPane(contentPane);
		//setModal(true);
		getRootPane().setDefaultButton(logInButton);

		logInButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				performLogin(usernameField.getText(), String.valueOf(passwordField.getPassword())); // Parse password as string, not char array.
			}
		});
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		Dimension screenResolution = Toolkit.getDefaultToolkit().getScreenSize();

		// Move this to it's own Constructor to access This.
		this.setTitle("Sandy Ambien Nights - V0.0.1");
		this.pack();
		this.setLocation((int)screenResolution.getWidth()/3, (int)screenResolution.getHeight()/3);
		this.setSize(300, 150);
		this.setVisible(true);
	}

	public static void main(String[] args) {
 		LoginView login = new LoginView();
	}

	public void performLogin(String username, String password){
		LoginController loginController = new LoginController();

		if(loginController.login(username, password)){
			// Logged in correctly.
			// Start full view & set variables.
			this.setVisible(false);
			new MenuView();
		} else {
			JOptionPane.showMessageDialog(null, "The data you entered were not correct. Please try again.");
		}
	}
}
