package GUIView;

import Model.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class CustomerView extends JPanel {
	private JPanel customerPanel;
	private JScrollPane customerScroll;
	private JTable customerTable;

	private JButton createButton;
	private JButton clearButton;
	private JTextField searchTextField;
	private JButton alleButton;
	ArrayList<CustomerModel> customerList;
	Map<Integer, CustomerModel> customers;
	DefaultTableModel defModel;
	String[] columnNames = { "Navn", "Addresse", "Email", "Telefon", "Kunde Type", "CVR", "Ændr", "Slet"};
	Object[][] data = {};
	int mapLength = 0;

	public CustomerView() {
		getAllCustomers();

		searchTextField.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if(searchTextField.getText().equals("Søg")){
					searchTextField.setText("");
				}
			}
		});

		// Create button.
		createButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				CustomerModel cc = customerPopup(null);
				if(cc != null){
                    CustomerCollectionModel.getInstance().addCustomer(cc); // Added to the "db"
				    customers.put(mapLength + 1, cc);
				    mapLength++;
				    getAllCustomers();
                }
			}
		});

		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<CustomerModel> foundCustomers = CustomerCollectionModel.getInstance().getListByKey(searchTextField.getText());
				if(foundCustomers == null){
					JOptionPane.showMessageDialog(null, "Ingen kunder fundet, prøv venligst igen!");
				} else {
					findCustomerList(foundCustomers);
				}
			}
		});

		alleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getAllCustomers();
			}
		});
	}

	public JPanel getCustomerPanel() {
		return customerPanel;
	}

	public void getAllCustomers(){
		customers = new HashMap<Integer, CustomerModel>();
		customerList = CustomerCollectionModel.getInstance().getAll();

		mapLength = 0;
		for(CustomerModel s : customerList){
			// Make a list of items that the user can select.
			customers.put(mapLength, s);
			mapLength++;
		}

		defModel = new DefaultTableModel(data, columnNames);
		customerTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
		customerTable.setModel(defModel);

		// Add the customers to the table.
		for(int i = 0; i < mapLength; i++){
			CustomerModel x = customers.get(i);
			defModel.addRow(new Object[]{x.getName(), x.getAddress(), x.getEmail(), x.getPhoneNumber(), x.getCustomerType(), x.getCvr(), "Ændr", "Slet"});
		}

		prepareButtons();
	}

	public void findCustomerList(ArrayList<CustomerModel> customerList){
		customers = new HashMap<Integer, CustomerModel>();

		mapLength = 0;
		for(CustomerModel s : customerList){
			// Make a list of items that the user can select.
			customers.put(mapLength, s);
			mapLength++;
		}

		defModel = new DefaultTableModel(data, columnNames);
		customerTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
		customerTable.setModel(defModel);

		// Add the customers to the table.
		for(int i = 0; i < mapLength; i++){
			CustomerModel x = customers.get(i);
			defModel.addRow(new Object[]{x.getName(), x.getAddress(), x.getEmail(), x.getPhoneNumber(), x.getCustomerType(), x.getCvr(), "Ændr", "Slet"});
		}

		prepareButtons();
	}

	public void prepareButtons(){
		// Delete action.
		Action delete = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable)e.getSource();
				Object[] options = {"Ja", "Nej"};
				JDialog frame = new JDialog();
				int n = JOptionPane.showOptionDialog(frame,
						"Er du sikker på at du vil fjerne denne kunde?",
						"Fjern kunde",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[0]);

				if(n == JOptionPane.YES_OPTION){
					int modelRow = Integer.valueOf( e.getActionCommand() );
					customerList.remove(customers.get(modelRow)); // Remove the item itself.
					// This should actually be done on the CONTROLLER! Or what? No it shouldn't.
					((DefaultTableModel)table.getModel()).removeRow(modelRow);
				}
			}
		};

		// Edit action.
		Action edit = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				int modelRow = Integer.valueOf( e.getActionCommand() );
				CustomerModel x = customers.get(modelRow);
				if(x != null) {
					CustomerModel y = customerPopup(x);

					if(y != null){ // Pressed OK!
						customers.put(modelRow, y); // DOES THIS UPDATE THE ARRAYLIST???  - no
						CustomerCollectionModel.getInstance().overwriteCustomer(x, y);

						// Update the line.
						defModel.setValueAt(y.getName(), modelRow, 0);
						defModel.setValueAt(y.getAddress(), modelRow, 1);
						defModel.setValueAt(y.getEmail(), modelRow, 2);
						defModel.setValueAt(y.getPhoneNumber(), modelRow, 3);
						defModel.setValueAt(y.getCustomerType(), modelRow, 4);
						defModel.setValueAt(y.getCvr(), modelRow, 5);
						defModel.fireTableDataChanged();
					}
				}
			}
		};

		TableColumn editColumn = customerTable.getColumn("Ændr");
		editColumn.setCellRenderer(new ButtonColumn(customerTable, edit, 6));
		TableColumn deleteColumn = customerTable.getColumn("Slet");
		deleteColumn.setCellRenderer(new ButtonColumn(customerTable, delete, 7)); // IF you are getting errors, these two are probably it.
	}


	public CustomerModel customerPopup(CustomerModel existingData){
		CustomerModel returnModel = null;

		Object[] options = {"Privat", "Erhverv"};
		JComboBox customerTypeSelect = new JComboBox(options);

		// Set everything to null.
		if(existingData == null){
			existingData = new CustomerModel("", "", "", "", "", "", "");
			customerTypeSelect.setSelectedIndex(0);
		} else {
			// Let's output ALL The fields!
			if(existingData.getCustomerType().equals("Privat")){
				customerTypeSelect.setSelectedIndex(0);
			} else {
				customerTypeSelect.setSelectedIndex(1);
			}

			customerTypeSelect.setSelectedIndex(0);
		}

		JTextField nameField = new JTextField(existingData.getName());
		JTextArea addressField = new JTextArea(existingData.getAddress());
		JTextField phoneField = new JTextField(existingData.getPhoneNumber());
		JTextField emailField = new JTextField(existingData.getEmail());
		JTextField cvrField = new JTextField(existingData.getCvr());
		Object[] message = {
				"Navn: ", nameField,
				"Addresse: ", addressField,
                "Phone: ", phoneField,
                "Email: ", emailField,
                "Kunde type: ", customerTypeSelect,
                "CVR: ", cvrField,
		};

		int option = JOptionPane.showConfirmDialog(getParent(), message, "Enter all your values", JOptionPane.OK_CANCEL_OPTION);

		if(option == JOptionPane.OK_OPTION) {
			String name = nameField.getText();
			String address = addressField.getText();
			String phone = phoneField.getText();
			String email = emailField.getText();
			String customerType = String.valueOf(customerTypeSelect.getSelectedItem());
			String cvr = cvrField.getText();

			// We have everything, it does need to be validated, but fuck that for now. - Create a Customer
			CustomerModel cc = new CustomerModel(name, address, email, phone, customerType, cvr, "");
			returnModel = cc;
		}

		return returnModel;
	}
}
