package GUIView;

import Model.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 * Created with IntelliJ IDEA.
 * User: alexanderbloch
 * Date: 09/06/13
 * Time: 18.25
 * To change this template use File | Settings | File Templates.
 */
public class AdministratorView extends JPanel{
    private JPanel administratorPanel;
    private JTable usersTable;
    private JButton createButton;
    private JButton clearButton;
    private JTextField searchTextField;
    private JButton alleButton;
    private JButton createButton2;
    private JTable locationsTable;

    ArrayList<UserModel> users;
    Map<Integer, UserModel> userMap;
    ArrayList<LocationModel> locations;
    Map<Integer, LocationModel> locationMap;
    DefaultTableModel defUserModel;
    DefaultTableModel defLocationModel;
    String[] userColumnNames = {"Navn", "Niveau", "Ændr", "Slet"};
    String[] locationColumnNames = {"Navn", "Ændr", "Slet"};
    Object[][] data = {};
    int mapLength = 0;


    public AdministratorView() {
        getAllUsers();
        getAllLocations();

        searchTextField.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                if(searchTextField.getText().equals("Søg")){
                    searchTextField.setText("");
                }
            }
        });

        // Create button.
         createButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                UserModel cc = userCreatePopup(null);
                if(cc != null){
                    UserCollectionModel.getInstance().addUser(cc); // Added to the "db"
                    userMap.put(mapLength + 1, cc);
                    mapLength++;
                    getAllUsers();
                }
            }
        });

        createButton2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                LocationModel lc = locationPopup(null);
                if(lc != null){
                    LocationCollectionModel.getInstance().addLocation(lc);
                    locationMap.put(mapLength + 1,lc);
                    mapLength++;
                    getAllLocations();
                }
            }
        });

      /*  clearButton.addActionListener(new ActionListener() {
                ArrayList<UserModel> foundUsers = UserCollectionModel.getInstance().getListByKey(searchTextField.getText());
                if(foundUsers == null){
                    JOptionPane.showMessageDialog(null, "Ingen brugere fundet, prøv venligst igen!");
                } else {
                    findUserList(foundUsers);
                }
        });  */

        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ArrayList<UserModel> foundUsers = UserCollectionModel.getInstance().getListByKey(searchTextField.getText());
                if(foundUsers == null){
                    JOptionPane.showMessageDialog(null, "Ingen brugere fundet, prøv venligst igen!");
                } else {
                    findUserList(foundUsers);
                }
            }
        });

        alleButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getAllUsers();
            }
        });

    }

    public void prepareUserButtons(){
        // Delete action.
        Action deleteUser = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable)e.getSource();
                Object[] options = {"Ja", "Nej"};
                JDialog frame = new JDialog();
                int n = JOptionPane.showOptionDialog(frame,
                        "Er du sikker på at du vil fjerne denne bruger",
                        "Fjern bruger",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]);

                if(n == JOptionPane.YES_OPTION){
                    int modelRow = Integer.valueOf( e.getActionCommand() );
                    users.remove(userMap.get(modelRow)); // Remove the item itself.
                    ((DefaultTableModel)table.getModel()).removeRow(modelRow);
                }
            }
        };


        // Edit action
        Action editUser = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int modelRow = Integer.valueOf( e.getActionCommand() );
                UserModel x = users.get(modelRow);
                String userType;
                if(x != null) {
                    Object[] options = {"Administrator", "Bruger"};
                    JComboBox accountTypeSelect = new JComboBox(options);

                    Object[] message = {
                        "Niveau: ", accountTypeSelect,
                    };

                    int option = JOptionPane.showConfirmDialog(getParent(), message, "Vælg niveau", JOptionPane.OK_CANCEL_OPTION);
                    if(option == JOptionPane.OK_OPTION) {
                        userType = String.valueOf(accountTypeSelect.getSelectedItem());
                    }
                    else{
                        userType = users.get(modelRow).getAccountType();
                    }
                    defUserModel.setValueAt(userType,modelRow,1);
                    users.get(modelRow).setAccountType(userType);
                    defUserModel.fireTableDataChanged();

                }
            }
        };



        // Ændr and slet buttons for usersPanel
        TableColumn editUserColumn = usersTable.getColumn("Ændr");
        editUserColumn.setCellRenderer(new ButtonColumn(usersTable, editUser, 2));
        editUserColumn.setMaxWidth(70);
        TableColumn deleteColumn = usersTable.getColumn("Slet");
        deleteColumn.setMaxWidth(70);
        deleteColumn.setCellRenderer(new ButtonColumn(usersTable, deleteUser, 3));

    }

    public void prepareLocationButtons(){
        Action deleteLocation = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable)e.getSource();
                Object[] options = {"Ja", "Nej"};
                JDialog frame = new JDialog();
                int n = JOptionPane.showOptionDialog(frame,
                        "Er du sikker på at du vil fjerne denne lokation",
                        "Fjern lokation,",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]);

                if (n == JOptionPane.YES_OPTION){
                    int modelRow = Integer.valueOf( e.getActionCommand() );
                    locations.remove(userMap.get(modelRow));
                    ((DefaultTableModel)table.getModel()).removeRow(modelRow);
                }


            }
        };

        Action editLocation = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int modelRow = Integer.valueOf( e.getActionCommand() );
                LocationModel x = locations.get(modelRow);
                if(x != null) {
                    LocationModel y = locationPopup(x);

                    if(y != null){ // Pressed OK!
                        locationMap.put(modelRow, y); // DOES THIS UPDATE THE ARRAYLIST???  - no
                        LocationCollectionModel.getInstance().overwriteLocation(x, y);

                        // Update the line.
                        defLocationModel.setValueAt(y.getName(), modelRow, 0);

                        defLocationModel.fireTableDataChanged();
                    }
                }
            }
        };


        // Ændre and slet buttons for locationsTable
        TableColumn editLocationColumn = locationsTable.getColumn("Ændr");
        editLocationColumn.setCellRenderer(new ButtonColumn(locationsTable, editLocation, 1));
        editLocationColumn.setMaxWidth(70);
        TableColumn deleteLocationColumn = locationsTable.getColumn("Slet");
        deleteLocationColumn.setCellRenderer(new ButtonColumn(locationsTable ,deleteLocation, 2));
        deleteLocationColumn.setMaxWidth(70);

    }

    public void findUserList(ArrayList<UserModel> userList){
        userMap = new HashMap<Integer, UserModel>();

        mapLength = 0;
        for(UserModel s : userList){
            // Make a list of items that the user can select.
            userMap.put(mapLength, s);
            mapLength++;
        }

        defUserModel = new DefaultTableModel(data, userColumnNames);
        usersTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
        usersTable.setModel(defUserModel);

        // Add the Users to the table.
        for(int i = 0; i < mapLength; i++){
            UserModel x = userMap.get(i);
            defUserModel.addRow(new Object[]{x.getName(), x.getAccountType(), "Ændr", "Slet"});
        }

        prepareUserButtons();
    }


    public void getAllUsers(){
        userMap = new HashMap<Integer, UserModel>();
        users = UserCollectionModel.getInstance().getAll();

        mapLength = 0;
        for(UserModel s : users){
            // Make a list of items that the user can select.
            userMap.put(mapLength, s);
            mapLength++;
        }

        defUserModel = new DefaultTableModel(data, userColumnNames);
        usersTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
        usersTable.setModel(defUserModel);

        // Add the users to the table.
        for(int i = 0; i < mapLength; i++){
            UserModel x = users.get(i);
            defUserModel.addRow(new Object[]{x.getName(), x.getAccountType(), "Ændr", "Slet"});
        }

        prepareUserButtons();
    }

    public void getAllLocations(){
        locationMap = new HashMap<Integer, LocationModel>();
        locations = LocationCollectionModel.getInstance().getAll();

        mapLength = 0;
        for(LocationModel s : locations){
            // Make a list of items that the user can select.
            locationMap.put(mapLength, s);
            mapLength++;
        }

        defLocationModel = new DefaultTableModel(data, locationColumnNames);
        locationsTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
        locationsTable.setModel(defLocationModel);

        // Add the users to the table.
        for(int i = 0; i < mapLength; i++){
            LocationModel x = locations.get(i);
            defLocationModel.addRow(new Object[]{x.getName(), "Ændr", "Slet"});
        }

        prepareLocationButtons();
        prepareUserButtons();
    }

    public UserModel userCreatePopup(UserModel existingData){
        UserModel returnModel = null;

        Object[] options = {"Administrator", "Bruger"};
        JComboBox accountTypeSelect = new JComboBox(options);

        // Set everything to null.
        if(existingData == null){
            existingData = new UserModel("", "", "", "");
            accountTypeSelect.setSelectedIndex(0);
        } else {
            // Let's output ALL The fields!
            if(existingData.getAccountType().equals("Administrator")){
                accountTypeSelect.setSelectedIndex(0);
            } else {
                accountTypeSelect.setSelectedIndex(1);
            }

            accountTypeSelect.setSelectedIndex(0);
        }
        JTextField usernameField = new JTextField(existingData.getUsername());
        JTextField nameField = new JTextField(existingData.getName());
        JTextField passwordField = new JTextField(existingData.getPassword());

        Object[] message = {
                "Username: ", usernameField,
                "Name: ", nameField,
                "Password: ", passwordField,
                "Niveau: ", accountTypeSelect,
        };

        int option = JOptionPane.showConfirmDialog(getParent(), message, "Enter all your values", JOptionPane.OK_CANCEL_OPTION);

        if(option == JOptionPane.OK_OPTION) {
            String username = usernameField.getText();
            String name = nameField.getText();
            String password = passwordField.getText();
            String userType = String.valueOf(accountTypeSelect.getSelectedItem());

            // We have everything, it does need to be validated, but fuck that for now. - Create a user
            UserModel cc = new UserModel(username, name, password, userType);
            returnModel = cc;
        }
        return returnModel;
    }

    public LocationModel locationPopup(LocationModel existingData){
        LocationModel returnModel = null;

        if(existingData == null){
            existingData = new LocationModel("");
        }

        JTextField nameField = new JTextField(existingData.getName());

        Object[] message = {
                "Name: ", nameField,
        };

        int option = JOptionPane.showConfirmDialog(getParent(), message, "Enter all your values", JOptionPane.OK_CANCEL_OPTION);

        if(option == JOptionPane.OK_OPTION) {
            String name = nameField.getText();

            // We have everything, it does need to be validated, but fuck that for now. - Create a user
            LocationModel cc = new LocationModel(name);
            returnModel = cc;
        }
        return returnModel;
    }

    public JPanel getAdministrationPanel(){
        return administratorPanel;
    }

}

