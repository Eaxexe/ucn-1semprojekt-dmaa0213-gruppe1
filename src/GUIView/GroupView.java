package GUIView;

import Model.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;

public class    GroupView {

	private JPanel groupPanel;
	private JTable groupTable;
	private JButton createButton;
	private JButton clearButton;
	private JTextField searchTextField;
	private JButton alleButton;
	ArrayList<GroupModel> groupList;
	HashMap<Integer, GroupModel> groups;
	DefaultTableModel defModel;
	String[] columnNames = { "Navn", "Id", "Antal Vare","Ændr", "Slet"};
	Object[][] data = {};
	Integer mapLength = 0;

	public GroupView() {
		getAllGroups();

		searchTextField.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if(searchTextField.getText().equals("Søg")){
					searchTextField.setText("");
				}
			}
		});

		// Create button.
		createButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				GroupModel cc = groupPopup(null);
				GroupCollectionModel.getInstance().addItem(cc); // Added to the "db"
				groups.put(mapLength + 1, cc);
				mapLength++;
				getAllGroups();
			}
		});

		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<GroupModel> foundGroups = GroupCollectionModel.getInstance().getListByKey(searchTextField.getText());
				if(foundGroups == null){
					JOptionPane.showMessageDialog(null, "Ingen grupper fundet, prøv venligst igen!");
				} else {
					finditemgroupList(foundGroups);
				}
			}
		});

		alleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getAllGroups();
			}
		});
	}

	public JPanel getGroupPanel() {
		return groupPanel;
	}

	public void getAllGroups(){
		groups = new HashMap<Integer, GroupModel>();
		groupList = GroupCollectionModel.getInstance().getAll();

		mapLength = 0;
		for(GroupModel s : groupList){
			// Make a list of items that the user can select.
			groups.put(mapLength, s);
			mapLength++;
		}

		defModel = new DefaultTableModel(data, columnNames);
		groupTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
		groupTable.setModel(defModel);

		// Add the customers to the table.
		for(int i = 0; i < mapLength; i++){
			GroupModel x = groups.get(i);
			defModel.addRow(new Object[]{x.getName(), x.getId(), x.getAntal(), "Ændr", "Slet"});
		}

		prepareButtons();
	}

	public void finditemgroupList(ArrayList<GroupModel> itemgroupList){
		groups = new HashMap<Integer, GroupModel>();

		mapLength = 0;
		for(GroupModel s : itemgroupList){
			// Make a list of items that the user can select.
			groups.put(mapLength, s);
			mapLength++;
		}

		defModel = new DefaultTableModel(data, columnNames);
		groupTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
		groupTable.setModel(defModel);

		// Add the customers to the table.
		for(int i = 0; i < mapLength; i++){
			GroupModel x = groups.get(i);
			defModel.addRow(new Object[]{x.getName(), x.getId(), x.getAntal(),"Ændr", "Slet"});
		}

		prepareButtons();
	}

	public void prepareButtons(){
		// Delete action.
		Action delete = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable)e.getSource();
				Object[] options = {"Ja", "Nej"};
				JDialog frame = new JDialog();
				int n = JOptionPane.showOptionDialog(frame,
						"Er du sikker på at du vil fjerne denne gruppe?",
						"Fjern gruppe",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[0]);

				if(n == JOptionPane.YES_OPTION){
					int modelRow = Integer.valueOf( e.getActionCommand() );
					groupList.remove(groups.get(modelRow)); // Remove the item itself.
					// This should actually be done on the CONTROLLER! Or what? No it shouldn't.
					((DefaultTableModel)table.getModel()).removeRow(modelRow);
				}
			}
		};

		// Edit action.
		Action edit = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				int modelRow = Integer.valueOf( e.getActionCommand() );
				GroupModel x = groups.get(modelRow);
				if(x != null) {
					GroupModel y = groupPopup(x);

					if(y != null){ // Pressed OK!
						groups.put(modelRow, y); // DOES THIS UPDATE THE ARRAYLIST???  - no
						GroupCollectionModel.getInstance().overwriteGroups(x, y);

						// Update the line.
						defModel.setValueAt(y.getName(), modelRow, 0);
						defModel.setValueAt(y.getId(), modelRow, 1);
						defModel.setValueAt(y.getAntal(), modelRow, 2);
						defModel.fireTableDataChanged();
					}
				}
			}
		};

		TableColumn editColumn = groupTable.getColumn("Ændr");
		editColumn.setCellRenderer(new ButtonColumn(groupTable, edit, 3));
		TableColumn deleteColumn = groupTable.getColumn("Slet");
		deleteColumn.setCellRenderer(new ButtonColumn(groupTable, delete, 4)); // IF you are getting errors, these two are probably it.
	}

	public GroupModel groupPopup(GroupModel existingData){
		GroupModel returnModel = null;

		// Set everything to null.
		if(existingData == null){
			existingData = new GroupModel("", "", 0);
		}
		JTextField nameField = new JTextField(existingData.getName());
		JTextField idField = new JTextField(existingData.getId());
		JTextField antalField = new JTextField(Integer.toString(existingData.getAntal()));
		Object[] message = {
				"Navn: ", nameField,
				"Id: ", idField,
				"Antal Varer: ", antalField,
		};

		int option = JOptionPane.showConfirmDialog(null, message, "Enter all your values", JOptionPane.OK_CANCEL_OPTION);

		if(option == JOptionPane.OK_OPTION) {
			String name = nameField.getText();
			String id = idField.getText();
			String antal = antalField.getText();

			// We have everything, it does need to be validated, but fuck that for now. - Create a Customer
			GroupModel cc = new GroupModel(name, id,Integer.parseInt(antal));
			returnModel = cc;
		}

		return returnModel;
	}
}
