package GUIView;

import Model.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class DealView extends JPanel {
    private JPanel dealPanel;
    private JScrollPane dealScroll;
    private JTable dealTable;

    private JButton createButton;
    private JButton clearButton;
    private JTextField searchTextField;
    private JButton alleButton;
    ArrayList<DealModel> dealList;
    Map<Integer, DealModel> deals;
    DefaultTableModel defModel;
    String[] columnNames = { "Navn", "Rabat Procent", "Spar x-antal Kr", "Ændr", "Slet"};
    Object[][] data = {};
    int mapLength = 0;

    public DealView() {
        getAllDeals();

        searchTextField.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                if(searchTextField.getText().equals("Søg")){
                    searchTextField.setText("");
                }
            }
        });

        // Create button.
        createButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                DealModel dl = dealPopup(null);
                DealCollectionModel.getInstance().addDeal(dl); // Added to the "db"
                deals.put(mapLength + 1, dl);
                mapLength++;
                getAllDeals();
            }
        });

        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ArrayList<DealModel> foundDeals = DealCollectionModel.getInstance().getListByKey(searchTextField.getText());
                if(foundDeals == null){
                    JOptionPane.showMessageDialog(null, "Ingen Kampagner fundet, prøv venligst igen!");
                } else {
                    findDealList(foundDeals);
                }
            }
        });

        alleButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getAllDeals();
            }
        });
    }

    public JPanel getDealPanels() {
        return dealPanel;
    }

    public void getAllDeals(){
        deals = new HashMap<Integer, DealModel>();
        dealList = DealCollectionModel.getInstance().getAll();

        mapLength = 0;
        for(DealModel s : dealList){
            // Make a list of items that the user can select.
            deals.put(mapLength, s);
            mapLength++;
        }

        defModel = new DefaultTableModel(data, columnNames);
        dealTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
        dealTable.setModel(defModel);

        // Add the customers to the table.
        for(int i = 0; i < mapLength; i++){
            DealModel x = deals.get(i);
            defModel.addRow(new Object[]{x.getName(), x.getDiscountPercentage(), x.getTotalPrice(), "Ændr", "Slet"});
        }

        prepareButtons();
    }

    public void findDealList(ArrayList<DealModel> dealList){
        deals = new HashMap<Integer, DealModel>();

        mapLength = 0;
        for(DealModel s : dealList){
            // Make a list of items that the user can select.
            deals.put(mapLength, s);
            mapLength++;
        }

        defModel = new DefaultTableModel(data, columnNames);
        dealTable.getTableHeader().setReorderingAllowed(false); // No dragging on the column names.
        dealTable.setModel(defModel);

        // Add the customers to the table.
        for(int i = 0; i < mapLength; i++){
            DealModel x = deals.get(i);
            defModel.addRow(new Object[]{x.getName(), x.getDiscountPercentage(), x.getTotalPrice(), "Ændr", "Slet"});
        }

        prepareButtons();
    }

    public void prepareButtons(){
        // Delete action.
        Action delete = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable)e.getSource();
                Object[] options = {"Ja", "Nej"};
                JDialog frame = new JDialog();
                int n = JOptionPane.showOptionDialog(frame,
                        "Er du sikker på at du vil fjerne denne Kampagne?",
                        "Fjern Kampagne",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]);

                if(n == JOptionPane.YES_OPTION){
                    int modelRow = Integer.valueOf( e.getActionCommand() );
                    dealList.remove(deals.get(modelRow)); // Remove the item itself.
                    // This should actually be done on the CONTROLLER! Or what? No it shouldn't.
                    ((DefaultTableModel)table.getModel()).removeRow(modelRow);
                }
            }
        };

        // Edit action.
        Action edit = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int modelRow = Integer.valueOf( e.getActionCommand() );
                DealModel x = deals.get(modelRow);
                if(x != null) {
                    DealModel y = dealPopup(x);

                    if(y != null){ // Pressed OK!
                        deals.put(modelRow, y); // DOES THIS UPDATE THE ARRAYLIST???  - no
                        DealCollectionModel.getInstance().overwriteDeal(x, y);

                        // Update the line.
                        defModel.setValueAt(y.getName(), modelRow, 0);
                        defModel.setValueAt(y.getDiscountPercentage(), modelRow, 1);
                        defModel.fireTableDataChanged();
                    }
                }
            }
        };

        TableColumn editColumn = dealTable.getColumn("Ændr");
        editColumn.setCellRenderer(new ButtonColumn(dealTable, edit, 3));
        TableColumn deleteColumn = dealTable.getColumn("Slet");
        deleteColumn.setCellRenderer(new ButtonColumn(dealTable, delete, 4)); // IF you are getting errors, these two are probably it.
    }


    public DealModel dealPopup(DealModel existingData){
        DealModel returnModel = null;

        // Set everything to null.
        if(existingData == null){
            existingData = new DealModel("","","");

        }

        JTextField nameField = new JTextField(existingData.getName());
        JTextField discField = new JTextField(existingData.getDiscountPercentage());
        JTextField totField = new JTextField(existingData.getTotalPrice());

        Object[] message = {
                "Navn: ", nameField,
                "Procent: ", discField,
                "Spar x-antal kr", totField,
        };

        int option = JOptionPane.showConfirmDialog(getParent(), message, "Enter all your values", JOptionPane.OK_CANCEL_OPTION);

        if(option == JOptionPane.OK_OPTION) {
            String name = nameField.getText();
            String discountPercentage = discField.getText();
            String totalPrice = totField.getText();

            // We have everything, it does need to be validated, but fuck that for now. - Create a Customer
            DealModel cc = new DealModel(name, discountPercentage, totalPrice);
            returnModel = cc;
        }

        return returnModel;
    }
}
