package CLIView;
import Model.*;
import Controller.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SalesView {

	public void menu() {
		Boolean exit = false;
		int choice;

		while(!exit){
			choice = menuChooser();

			switch(choice){
				case 1:
					listSales();
					break;
				case 2:
					createSale();
					break;
                case 3:
                    removeItemFromSale();
                    break;
                /*case 4:
                    clearSale();
                    break; */
				case 9:
					exit = true;
					break;
			}
		}
	}

	public int menuChooser() {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("- Sales -");
		System.out.println("1) List sales");
		System.out.println("2) Create sale");
        System.out.println("3) Remove Item from Sale");
        System.out.println("4) Clear Sale");
		System.out.println("9) Exit");
		System.out.print("> ");

		return keyboard.nextInt();
	}

	public void createSale() {
		int date = (int) (System.currentTimeMillis() / 1000L);
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		int itemAmount = 0;
		double total = 0;
		Boolean hasPaid = true;
		Boolean exit = false;

		ArrayList<ItemModel> itemsList = ItemCollectionModel.getInstance().getAll();
		Map<Integer, ItemModel> items = new HashMap<Integer, ItemModel>();
		ArrayList<SalesItemModel> selectedItems = new ArrayList<SalesItemModel>();

		// Make a list of items that the user can select.
		int n = 1;
		for(ItemModel i : itemsList){
			items.put(n, i);
			n++;
		}

		while(!exit){
			while(choice <= 0 || choice > n){
				for(int i = 1; i < n; i++){
					System.out.println(i+") "+items.get(i).getName());
				}
				System.out.println(n+") Finished");
				System.out.println("Please pick an item: ");
				System.out.print("> ");
				choice = keyboard.nextInt();
			}

			if(choice != n){
				System.out.println("Amount:");
				System.out.print("> ");
				itemAmount = keyboard.nextInt();

				System.out.println("You chose: "+items.get(choice).getName()+" x "+itemAmount+" - It has been added\n\n");
				// Add to list.
				SalesItemModel temp = new SalesItemModel(items.get(choice), items.get(choice).getPrice(), itemAmount);
				selectedItems.add(temp);
				total += items.get(choice).getPrice();
				itemAmount = 0; // Reset it.
				choice = 0;
			} else {
				exit = true;
			}
		}
		SalesController sales = new SalesController();
		sales.create(total, date, hasPaid, selectedItems);
		System.out.println("Sale has been added to system.");
	}

	public void listSales() {
		SalesController salesController = new SalesController();

		ArrayList<SalesModel> sales = salesController.getAll();
		if(sales.size() > 0){
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

			for(SalesModel s : sales){
				System.out.println("X\tSale by:\t\t"+s.getUser().getName());
				System.out.println("X\tThe total price is:\t"+s.getTotalPrice());
				if(s.getPaid()){
					System.out.println("X\tPaid: Yes");
				} else {
					System.out.println("X\tPaid: No");
				}

				System.out.println("Items in sale: ");

				for(SalesItemModel i : s.getSalesItems()){
					System.out.println("\tX\tName: "+i.getItem().getName());
					System.out.println("\tX\tAmount: "+i.getAmount());
					System.out.println("\tX\tPrice: "+i.getSalesPrice());
					System.out.println();
				}

				System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
			}
		} else {
			System.out.println("No sales yet.");
		}
	}

    public void removeItemFromSale(){
        SalesController salesController = new SalesController();
        ItemController itemController = new ItemController();
        int choice = 0;
        Scanner keyboard = new Scanner(System.in);


        ArrayList<SalesItemModel> salesItems = salesController.list();
        Map<Integer, SalesItemModel> saleItem = new HashMap<Integer, SalesItemModel>();

        int n = 1;
        for(SalesItemModel i : salesItems){
            saleItem.put(n, i);
            n++;
        }

        if(saleItem != null ){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            while(choice <= 0 || choice > n){
                for(int i = 1; i < n; i++){
                    System.out.println(i + ") " + saleItem.get(i).getItem());
                }
                System.out.println(n+") Exit");
                System.out.println("Please pick an Item: ");
                System.out.print("> ");
                choice = keyboard.nextInt();

                if(choice != n){
                    saleItem.remove(saleItem.get(choice));
                    System.out.println(saleItem.get(choice).getItem()+" has been removed");
                }
            }
        }
    }
}