package CLIView;
import Model.*;
import Controller.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.ArrayList;

public class CustomerView {

	public void menu() {

		Boolean exit = false;
		int choice;

		while(!exit){
			choice = menuChooser();

			switch(choice){
				case 1:
					createCustomer();
					break;
				case 2:
					findCustomer();
					break;
                case 3:
                    listCustomer();
                    break;
                case 4:
                    editCustomer();
                    break;
                case 5:
                    removeCustomer();
                    break;
				case 9:
					exit = true;
					break;
			}
		}
	}

	public int menuChooser() {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("- Customers -");
		System.out.println("1) Add customer");
		System.out.println("2) Find customer");
        System.out.println("3) List customer");
        System.out.println("4) Edit customer");
		System.out.println("5) Remove customer");
		System.out.println("9) Exit");
		System.out.print("> ");

		return keyboard.nextInt();
	}

	public void createCustomer() {
		CustomerController customerController = new CustomerController();
		Scanner keyboard = new Scanner(System.in); // take input.
		String name, address, email, phoneNumber, userInput = "0", userCvr, discountType, customerType;
        int cvr = 0;

        System.out.print("Please enter name: ");
		name = keyboard.nextLine();
		System.out.print("Please enter address: ");
		address = keyboard.nextLine();
        System.out.print("Please enter email: ");
        email = keyboard.nextLine();
        System.out.print("Please enter phone number: ");
        phoneNumber = keyboard.nextLine();
        System.out.print("Please enter customer type: ");

        while(true) {
            userInput = keyboard.nextLine();
            if((userInput.toLowerCase().equals("erhverv")) || (userInput.toLowerCase().equals("privat"))){
                customerType = userInput;
                break;
            }
            else{
                System.out.print("Input is invalid. Please enter either Privat or Erhverv: ");
            }
        }
        System.out.print("Please enter cvr: ");
        userCvr = keyboard.nextLine();

        /*while((!isInteger(userCvr)) || (userCvr.length() != 8)){
            System.out.println("Input can only contain numbers and has to contain 8 numbers.");
            System.out.print("Please enter cvr: ");
            userCvr = keyboard.nextLine();
            if(isInteger(userCvr) && (userCvr.length() == 8)){
                cvr = Integer.parseInt(userCvr);
                break;
            }
        }*/

        System.out.print("Please enter discount type: ");
        discountType = keyboard.nextLine();

        // Add the user to the list.
        customerController.create(name, address, email, phoneNumber, customerType, userCvr, discountType);
	}

	public void findCustomer() {
		CustomerController customerController = new CustomerController();
		Scanner keyboard = new Scanner(System.in); // take input.

		System.out.print("Please enter search string: ");
		String key = keyboard.nextLine();

		CustomerModel c = customerController.find(key);

		if(c != null){
			System.out.println("Found a customer by that info:");
			System.out.println("Name: "+c.getName());
			System.out.println("Address: "+c.getAddress());
            System.out.println("Email: "+c.getEmail());
            System.out.println("Phone number: "+c.getPhoneNumber());
            System.out.println("Customer type: "+c.getCustomerType());
            System.out.println("CVR: "+c.getCvr());
            System.out.println("Discount type: "+c.getDiscountType());
		} else {
			System.out.println("Did not find user.");
		}
    }

    // Checks if a string contains only integers
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public void listCustomer() {
        CustomerController customerController = new CustomerController();

        ArrayList<CustomerModel> customer = customerController.getAll();
                if(customer != null ){
                    System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                    for(CustomerModel u : customer){
                        System.out.println("X\tName:\t\t"+u.getName());
                        System.out.println("X\tId:\t"+u.getAddress());
                        System.out.println("X\tType:\t\t"+u.getPhoneNumber());
                        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                    }
                }
    }

    public void editCustomer() {
        boolean exit = false;
        int choice = 0;
        String userInput, customerType, cvr;
        Scanner keyboard = new Scanner(System.in);


        CustomerController customerController = new CustomerController();
        CustomerModel c = getCustomer();

        if (c != null){


            while(!exit){
                System.out.println("Found a customer by that info:");

                System.out.println("1) Name:\t\t\t"+c.getName());
                System.out.println("2) Address:\t\t\t"+c.getAddress());
                System.out.println("3) Email:\t\t\t"+c.getEmail());
                System.out.println("4) Phone number: \t"+c.getPhoneNumber());
                System.out.println("5) Customer type: \t"+c.getCustomerType());
                System.out.println("6) CVR:\t\t\t\t"+c.getCvr());
                System.out.println("Please select field to edit.");
                System.out.print("> ");

                while (!keyboard.hasNextInt()) {
                    System.out.println("\n### ERROR ### ");
                    System.out.println("### Please enter a number ### ");

                    keyboard.nextLine();
                }
                choice = keyboard.nextInt();

                switch(choice){
                    case 1:
                        System.out.println("Set name to: ");
                        c.setName(editField());
                        break;
                    case 2:
                        System.out.println("Set address to: ");
                        c.setAddress(editField());
                        break;
                    case 3:
                        System.out.println("Set email to: ");
                        c.setEmail(editField());
                        break;
                    case 4:
                        System.out.println("Set phone number to: ");
                        c.setPhoneNumber(editField());
                        break;
                    case 5:
                        System.out.println("Set customer type to: ");
                        System.out.print("> ");
                        while(true) {
                            userInput = keyboard.nextLine();
                            if((userInput.toLowerCase().equals("privat")) || (userInput.toLowerCase().equals("erhverv"))){
                                customerType = userInput;
                                break;
                            }
                            else{
                                System.out.print("Forkert indtastning. skriv Privat eller Erhverv: ");
                            }
                        }
                        c.setCustomerType(customerType);
                        break;
                    case 6:
                        System.out.println("Set cvr to: ");
                        System.out.print("> ");
                        userInput = keyboard.nextLine();

                        /*while((!isInteger(userInput)) || (userInput.length() != 8)){
                            System.out.println("Input can only contain numbers and has to contain 8 numbers.");
                            System.out.print("Please enter cvr: ");
                            userInput = keyboard.nextLine();
                            if(isInteger(userInput) && (userInput.length() == 8)){
                                cvr = Integer.parseInt(userInput);
                                c.setCvr(;
                                break;
                            }
                        }     */
                        break;

                }
                c = customerController.find(c.getName());
            }
        }
    }

    public String editField() {
        System.out.print("> ");
        Scanner keyboard = new Scanner(System.in); // take input.
        return keyboard.nextLine();
    }

    public CustomerModel getCustomer(){
        CustomerController customerController = new CustomerController();
        Scanner keyboard = new Scanner(System.in); // take input.

        System.out.print("Please enter search string: ");
        String key = keyboard.nextLine();

        return customerController.find(key);
    }


    public void removeCustomer() {
        int choice = 0;
        Scanner keyboard = new Scanner(System.in);
        CustomerController customerController = new CustomerController();

        ArrayList<CustomerModel> customers = customerController.getAll();
        Map<Integer, CustomerModel> customer = new HashMap<Integer, CustomerModel>();

        int n = 1;
        for(CustomerModel i : customers){
            customer.put(n, i);
            n++;
        }

        if(customers != null ){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            while(choice <= 0 || choice > n){
                for(int i = 1; i < n; i++){
                    System.out.println(i+") "+customers.get(i).getName());
                }
                System.out.println(n+") Exit");
                System.out.println("Please pick a location: ");
                System.out.print("> ");
                choice = keyboard.nextInt();

                if(choice != n){
                    customers.remove(customer.get(choice));
                    System.out.println(customer.get(choice).getName()+" has been removed");
                }
            }
        }
    }
}