package CLIView;
import Controller.*;
import java.util.Scanner;

public class LoginView {

	public static void main(String[] args) {
		/*MainCLIView m = new MainCLIView();
		m.menu();*/
		// First of all, make a user so we can actually test and log in.
		UserController userController = new UserController();
		userController.create("eax", "Eax Exe", "erhot", "svin");
        userController.create("alekon", "Alex", "willy", "champ");
		ItemController itemController = new ItemController();
		itemController.create("Dyknagler 10stk", "DKNGLR", 150, 500, 50, 150, null, null, null);
		itemController.create("Hampereb 5m", "HMPRB", 200, 50, 10, 15, null, null, null);
        itemController.create("Steppeløber 10K", "STPLB", 125, 25, 50, 20, null, null, null);

		LoginView loginView = new LoginView();
		loginView.login();
		MenuView menuView = new MenuView();
		menuView.menu();
	}

	public void login() {
		LoginController loginController = new LoginController();

		Scanner keyboard = new Scanner(System.in);
		String username;
		String password;

		System.out.println("Welcome to Sandy Ambien Nights");
		while(true){
			System.out.println("Please log in:");
			System.out.print("Username: ");
			username = keyboard.nextLine();
			System.out.print("Password: ");
			password = keyboard.nextLine();

			if(loginController.login(username, password)){
				System.out.println("\n\n\n");
				System.out.println("Welcome online Commander.");
				break;
			} else {
				System.out.print("XXX ACCESS DENIED XXX");
			}
		}
	}
}