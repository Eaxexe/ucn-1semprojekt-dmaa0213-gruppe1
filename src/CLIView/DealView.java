package CLIView;
import Model.*;
import Controller.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DealView {

    public void menu() {
        Boolean exit = false;
        int choice;

        while(!exit){
            choice = menuChooser();

            switch(choice){
                case 1:
                    createDeal();
                    break;
                case 2:
                    findDeal();
                    break;
                case 3:
                    listDeal();
                    break;
                case 4:
                    removeDeal();
                    break;
                case 9:
                    exit = true;
                    break;
            }
        }
    }

    public int menuChooser() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("- Deal -");
        System.out.println("1) Add Deal");
        System.out.println("2) Find Deal");
        System.out.println("3) List Deal");
        System.out.println("4) Remove Deal");
        System.out.println("9) Exit");
        System.out.print("> ");

        return keyboard.nextInt();
    }

    public void createDeal() {
        DealController dealController = new DealController();
        Scanner keyboard = new Scanner(System.in); // take input.
        String name;
        String discountPercentage;
        String totalPrice;

        System.out.print("Please enter name: ");
        name = keyboard.nextLine();
        System.out.print("Please enter discountPercentage: ");
        discountPercentage = keyboard.nextLine();
        while((!isInteger(discountPercentage)) || Integer.parseInt(discountPercentage) < 1 || Integer.parseInt(discountPercentage) > 100){
            System.out.println("Can only contain numbers 1-100");
            System.out.print("Please enter discountPercentage: ");
            discountPercentage = keyboard.nextLine();
        }
        System.out.println("Please enter totalPrice: ");
        totalPrice = keyboard.nextLine();
        while((!isInteger(totalPrice))){
            System.out.println("Can only obtain numbers");
            System.out.print("Please enter totalPrice: ");
            totalPrice = keyboard.nextLine();
        }
        dealController.create(name, discountPercentage, totalPrice);
    }

    public void findDeal() {
        DealController dealController = new DealController();
        Scanner keyboard = new Scanner(System.in); // take input.

        System.out.print("Please enter search string: ");
        String key = keyboard.nextLine();

        DealModel d = dealController.find(key);

        if(d != null){
            System.out.println("Found a deal by that info:");
            System.out.println("Name: " + d.getName());
            System.out.println("DiscountPercentage: " + d.getDiscountPercentage());
            System.out.println("Total Price: " + d.getTotalPrice());
        } else {
            System.out.println("Did not find deal.");
        }
    }
    public void listDeal() {
        DealController dealController = new DealController();

        ArrayList<DealModel> deal = dealController.getAll();
        if(deal != null ){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            for(DealModel u : deal){
                System.out.println("X\tName:\t\t"+u.getName());
                System.out.println("X\tId:\t"+u.getTotalPrice());
                System.out.println("X\tType:\t\t"+u.getDiscountPercentage());
                System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            }
        }
    }

    public void removeDeal() {
        int choice = 0;
        DealController dealController = new DealController();
        Scanner keyboard = new Scanner(System.in);

        ArrayList<DealModel> deals = dealController.getAll();
        Map<Integer, DealModel> deal = new HashMap<Integer, DealModel>();

        int n = 1;
        for(DealModel i : deals){
            deal.put(n, i);
            n++;
        }

        if(deals.size() > 0){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            while(choice <= 0 || choice > n){
                for(int i = 1; i < n; i++){
                    System.out.println(i+") "+deal.get(i).getName());
                }
                System.out.println(n+") Exit");
                System.out.println("Please pick a group: ");
                System.out.print("> ");
                choice = keyboard.nextInt();

                if(choice != n){
                    deals.remove(deal.get(choice));
                    System.out.println(deal.get(choice).getName()+" has been removed");
                }
            }
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}