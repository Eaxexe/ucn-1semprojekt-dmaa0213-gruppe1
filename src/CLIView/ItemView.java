package CLIView;
import Model.*;
import Controller.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ItemView {

	public void menu(){
		Boolean exit = false;
		int choice;

		while(!exit){
			choice = menuChooser();

			switch(choice){
				case 1:
					createItem();
					break;
				case 2:
					findItem();
					break;
				case 3:
                    listItem();
                    break;
                case 4:
                    editItem();
                    break;
                case 9:
					exit = true;
					break;
			}
		}
	}

	public int menuChooser(){
		Scanner keyboard = new Scanner(System.in);
		System.out.println("- Items -");
		System.out.println("1) Add item");
		System.out.println("2) Find item");
        System.out.println("3) List items");
        System.out.println("4) Edit item");
        System.out.println("9) Exit");
		System.out.print("> ");

		return keyboard.nextInt();
	}

	public void createItem(){
		ItemController itemController = new ItemController();
        GroupController groupController = new GroupController();
        LocationController locationController = new LocationController();
        ArrayList<ItemLocationModel> yolo = new ArrayList<ItemLocationModel>();
		Scanner keyboard = new Scanner(System.in); // take input.
		String name;
		String id;
        double price;
        int maximumAmount;
        int minimumAmount;
        int currentAmount;
        GroupModel group;
        ItemLocationModel itemLocationModel;
        ItemModel itemModel = null;
        LocationModel locationModel = null;

		System.out.print("Please enter name: ");
		name = keyboard.nextLine();

        System.out.print("Please enter id: ");
        id = keyboard.nextLine();

        System.out.print("Please enter price: ");
        price = keyboard.nextDouble();

        System.out.print("Please enter maximum amount: ");
        maximumAmount = keyboard.nextInt();

        System.out.print("Please enter minimum amount: ");
        minimumAmount = keyboard.nextInt();

        System.out.print("Please enter current amount: ");
        currentAmount = keyboard.nextInt();

        /*System.out.print("Please enter group: ");
        String key = keyboard.nextLine();
        GroupModel g = groupController.find(key);
        while(true){
            key = keyboard.nextLine();
            g = groupController.find(key);

            if(g != null){
               group = groupController.find(key);
               break;
            }
            else {
                System.out.println("Did not find group.");
                System.out.print("Please enter a new one: ");
            }
        }   */
		group = null;

         /*System.out.println("Please enter Location");
         LocationModel l = locationController.find(key);
         while(true){
             key = keyboard.nextLine();
             l = locationController.find(key);
             if(l != null){
                 locationModel = locationController.find(key);
                 break;
             }else {
                 System.out.println("Did not find location");
                 System.out.println("Please enter a new location");
             }
         }     */


         itemController.create(name, id, price, maximumAmount, minimumAmount, currentAmount, group, yolo, null);
	}

	public void findItem(){
		ItemController itemController = new ItemController();
		Scanner keyboard = new Scanner(System.in); // take input.

		System.out.print("Please enter search string: ");
		String key = keyboard.nextLine();

		ItemModel i = itemController.find(key);

		if(i != null){
			System.out.println("Found a item by that info:");
			System.out.println("Name: "+i.getName());
			System.out.println("Id: "+i.getId());
            System.out.println("Price: "+i.getPrice());
            System.out.println("Maximum amount: "+i.getMaximumAmount());
            System.out.println("Minimum amount: "+i.getMinimumAmount());
            System.out.println("Current amount: "+i.getCurrentAmount());
            //System.out.println("Group: "+i.getGroup()); // We have more than one.
            //System.out.println("Location: "+i.getLocation()); // We have more than one.
		} else {
			System.out.println("Did not find Item.");
		}
	}

    public void listItem(){
        ItemController itemController = new ItemController();

        ArrayList<ItemModel> items = itemController.getAll();
        if(items != null ){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            for(ItemModel u : items){
                System.out.println("X\tName:\t\t"+u.getName());
                System.out.println("X\tId:\t"+u.getId());
                System.out.println("X\tType:\t\t"+u.getPrice());
                System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            }
        }
    }



    public void editItem(){
        // Find item.
        // Have small menu to edit different parts.
        ItemController itemController = new ItemController();
        ItemModel c = null;
        GroupModel d = null;
        Boolean exit = false;
        int choice = 0;
        Scanner keyboard = new Scanner(System.in); // take input.

        if(c != null){
            System.out.println("Found an item by that info:");

            while(!exit){
                System.out.println("1) Item name:\t\t"+c.getName());
                System.out.println("2) Item ID:\t"+c.getId());
                System.out.println("3) Price: \t\t"+c.getPrice());
                System.out.println("4) Current Amount: \t\t"+c.getCurrentAmount());
                System.out.println("5) Minimum Amount: \t\t"+c.getMinimumAmount());
                System.out.println("6) Maximum Amount: \t\t"+c.getMaximumAmount());
                /*System.out.println("7) add Group: \t\t"+c.getGroup()); // More than one.
                System.out.println("8) add Location: \t\t"+c.getLocation());
                System.out.println("9) Remove Group: \t\t"+c.getGroup());
                System.out.println("10) Remove Location: \t\t"+c.getLocation());*/
                System.out.println("11) Exit");
                System.out.println("Please select field to edit.");
                System.out.print("> ");

                while (!keyboard.hasNextInt()) {
                    System.out.println("\n### ERROR ### ");
                    System.out.println("### Please enter a number ### ");

                    keyboard.nextLine();
                }
                choice = keyboard.nextInt();

                switch(choice){
                    case 1:
                        System.out.println("Set Item Name to: ");
                        c.setName(editField());
                        break;
                    case 2:
                        System.out.println("Set ID to: ");
                        c.setId(editField());
                        break;
                    case 3:
                        System.out.println("Set Price to: ");
                        c.setPrice(editDouble());
                        break;
                    case 4:
                        System.out.println("Set current Amount to: ");
                        c.setCurrentAmount(editInt());
                        break;
                    case 5:
                        System.out.println("Set minimum Amount to: ");
                        c.setCurrentAmount(editInt());
                        break;
                    case 6:
                        System.out.println("Set maximum Amount to: ");
                        c.setCurrentAmount(editInt());
                        break;
                    case 7:
                        System.out.println("Add Item Group to: ");
                        addGroup();
                        break;
                    case 8:
                        System.out.println("add Location: ");
                        addLocation();
                        break;
                    case 9:
                        System.out.println("Remove group: ");
                        removeGroup();
                        break;
                    case 10:
                        System.out.println("Remove Location: ");
                        removeLocation();
                        break;
                    case 11:
                        exit = true;
                        break;
                }

                // Find the Item again - This way we get updated data.
                c = itemController.find(c.getName());
            }
        } else {
            System.out.println("No Item has been found.");
        }
    }

    // Catch-all method to set the fields of an Item being edited.
    public String editField(){
        System.out.print("> ");
        Scanner keyboard = new Scanner(System.in); // take input.
        return keyboard.nextLine();
    }
    public double editDouble(){
        System.out.print("> ");
        Scanner keyboard = new Scanner(System.in);
        return keyboard.nextDouble();
    }
    public int editInt(){
        System.out.print("> ");
        Scanner keyboard = new Scanner(System.in);
        return keyboard.nextInt();
    }

    public void addGroup(){
        System.out.println("> ");
        Scanner keyboard = new Scanner(System.in);
        int choice = 0;
        Boolean exit = false;

        ArrayList<GroupModel> groupList = GroupCollectionModel.getInstance().getAll();
        Map<Integer, GroupModel> groups = new HashMap<Integer, GroupModel>();
        //ArrayList<ItemGroupModel> selectedGroups = new ArrayList<ItemGroupModel>();

        int n = 1;
        for(GroupModel i : groupList){
            groups.put(n, i);
            n++;
    }

    while(!exit){
        while(choice <= 0 || choice > n){
            for(int i = 1; i < n; i++){
                System.out.println(i+") "+groups.get(i).getName());
            }
            System.out.println(n+") Finished");
            System.out.println("Please pick a group: ");
            System.out.print("> ");
            choice = keyboard.nextInt();

         }  if(choice != n){
            System.out.println("You chose: "+groups.get(choice).getName()+" - It has been added\n\n");
            // Add to list.
            //ItemGroupModel temp = new ItemGroupModel(groups.get(choice));
            //selectedGroups.add(temp);
            choice = 0;
        } else {
            exit = true;
        }
    }}

    public void addLocation(){
        System.out.println("> ");
        Scanner keyboard = new Scanner(System.in);
        int choice = 0;
        Boolean exit = false;
        LocationController locationController = new LocationController();
        ItemModel yolo = null;

        ArrayList<ItemLocationModel> itemLocationList = yolo.getAllItemLocations();
        Map<Integer,ItemLocationModel> locations = new HashMap<Integer, ItemLocationModel>();
        ArrayList<ItemLocationModel> selectedLocation = new ArrayList<ItemLocationModel>();

        int n = 1;
        for(ItemLocationModel i : itemLocationList){
            locations.put(n, i);
            n++;
        }

        while(!exit){
            while(choice <= 0 || choice > n){
                for(int i = 1; i < n; i++){
                    System.out.println(i+") "+locations.get(i).getLocation());
                }
                System.out.println(n+") Finished");
                System.out.println("Please pick a location: ");
                System.out.print("> ");
                choice = keyboard.nextInt();

            }  if(choice != n){
                System.out.println("You chose: "+locations.get(choice).getLocation()+" - It has been added\n\n");
                // Add to list.
                //ItemLocationModel temp = new ItemLocationModel(locations.get(choice)   ), locations.get(choice).getLocation(), locations.get(choice).getShelf(), locations.get(choice).getAmount());
                //selectedLocation.add(temp);
                choice = 0;
            } else {
                exit = true;
            }
        }

    }
    public void removeGroup(){
        int choice = 0;
        Scanner keyboard = new Scanner(System.in);
        ItemController itemController = new ItemController();

        ArrayList<GroupModel> groups = itemController.listGroups();
        Map<Integer, GroupModel> group = new HashMap<Integer, GroupModel>();

        int n = 1;
        for(GroupModel i : groups){
            group.put(n, i);
            n++;
        }

        if(group != null ){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            while(choice <= 0 || choice > n){
                for(int i = 1; i < n; i++){
                    System.out.println(i+") "+group.get(i).getName());
                }
                System.out.println(n+") Exit");
                System.out.println("Please pick a group: ");
                System.out.print("> ");
                choice = keyboard.nextInt();

                if(choice != n){
                    group.remove(group.get(choice));
                    System.out.println(group.get(choice).getName()+" has been removed");
                }
            }
        }
    }
    public void removeLocation(){
        int choice = 0;
        Scanner keyboard = new Scanner(System.in);
        ItemController itemController = new ItemController();

        ArrayList<LocationModel> locations = itemController.listLocations();
        Map<Integer, LocationModel> location = new HashMap<Integer, LocationModel>();

        int n = 1;
        for(LocationModel i : locations){
            location.put(n, i);
            n++;
        }

        if(location != null ){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            while(choice <= 0 || choice > n){
                for(int i = 1; i < n; i++){
                    System.out.println(i+") "+location.get(i).getName());
                }
                System.out.println(n+") Exit");
                System.out.println("Please pick a location: ");
                System.out.print("> ");
                choice = keyboard.nextInt();

                if(choice != n){
                    location.remove(location.get(choice));
                    System.out.println(location.get(choice).getName()+" has been removed");
                }
            }
        }
    }
}
