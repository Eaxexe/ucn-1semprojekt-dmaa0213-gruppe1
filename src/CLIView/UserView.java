package CLIView;
import Model.*;
import Controller.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.ArrayList;

public class UserView {

	public void menu() {
		Boolean exit = false;
		int choice;

		while(!exit){
			choice = menuChooser();

			switch(choice){
				case 1:
					createUser();
					break;
				case 2:
					findUser();
					break;
				case 3:
					findUsers();
					break;
				case 4:
					listUsers();
					break;
				case 5:
					editUser();
					break;
                case 6:
                    removeUser();
                    break;
				case 9:
					exit = true;
					break;
			}
		}
	}

	public int menuChooser() {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("- Users -");
		System.out.println("1) Add user");
		System.out.println("2) Find user");
		System.out.println("3) Find users");
		System.out.println("4) List all users");
		System.out.println("5) Edit a user");
        System.out.println("6) Remove user");
		System.out.println("9) Exit");
		System.out.print("> ");

		return keyboard.nextInt();
	}

	public void createUser() {
		UserController userController = new UserController();
		Scanner keyboard = new Scanner(System.in); // take input.
		String username;
		String name;
		String password;
		String accountType = null;

		System.out.print("Please enter username: ");
		username = keyboard.nextLine();
		System.out.print("Please enter full name: ");
		name = keyboard.nextLine();
		System.out.print("Please enter password: ");
		password = keyboard.nextLine();

		accountType = setAccountType();

		// Add the user to the list.
		userController.create(username, name, password, accountType);

		System.out.print("User created.");
	}

	public void findUser() {
		UserModel c = getUser();

		if(c != null){
			System.out.println("Found a user by that info:");
			System.out.println("Username:\t\t"+c.getUsername());
			System.out.println("Name:\t\t\t"+c.getName());
			System.out.println("Account Type:\t"+c.getAccountType());
			System.out.println("Password: \t\t"+c.getPassword());
		} else {
			System.out.println("No user has been found.");
		}
	}

	public void findUsers() {
		UserController userController = new UserController();
		Scanner keyboard = new Scanner(System.in); // take input.

		System.out.print("Please enter search string: ");
		String key = keyboard.nextLine();

		ArrayList<UserModel> users = userController.findList(key);
		if(users != null ){
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

			for(UserModel u : users){
				System.out.println("X\tName:\t\t"+u.getName());
				System.out.println("X\tUsername:\t"+u.getUsername());
				System.out.println("X\tType:\t\t"+u.getAccountType());
				System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
			}
		}
	}

	public void listUsers() {
		UserController userController = new UserController();

		ArrayList<UserModel> users = userController.getAll();
		if(users != null ){
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

			for(UserModel u : users){
				System.out.println("X\tName:\t\t"+u.getName());
				System.out.println("X\tUsername:\t"+u.getUsername());
				System.out.println("X\tType:\t\t"+u.getAccountType());
				System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
			}
		}
	}

	public void editUser() {
		// Find user.
		// Have small menu to edit different parts.
		UserController userController = new UserController();
		UserModel c = getUser();
		Boolean exit = false;
		int choice = 0;
		Scanner keyboard = new Scanner(System.in); // take input.

		if(c != null){
			System.out.println("Found a user by that info:");

			while(!exit){
				System.out.println("1) Username:\t\t"+c.getUsername());
				System.out.println("2) Name:\t\t\t"+c.getName());
				System.out.println("3) Account Type:\t"+c.getAccountType());
				System.out.println("4) Password: \t\t"+c.getPassword());
				System.out.println("5) Exit");
				System.out.println("Please select field to edit.");
				System.out.print("> ");

				while (!keyboard.hasNextInt()) {
					System.out.println("\n### ERROR ### ");
					System.out.println("### Please enter a number ### ");

					keyboard.nextLine();
				}
				choice = keyboard.nextInt();

				switch(choice){
					case 1:
						System.out.println("Set username to: ");
						c.setName(editField());
						break;
					case 2:
						System.out.println("Set name to: ");
						c.setUsername(editField());
						break;
					case 3:
						c.setAccountType(setAccountType());
						break;
					case 4:
						System.out.println("Set password to: ");
						c.setPassword(editField());
						break;
					case 5:
						exit = true;
						break;
				}

				// Find the user again.
				c = userController.find(c.getUsername());
			}
		} else {
			System.out.println("No user has been found.");
		}
	}

	// Catch-all method to set the fields of a user being edited.
	public String editField() {
		System.out.print("> ");
		Scanner keyboard = new Scanner(System.in); // take input.
		return keyboard.nextLine();
	}

	// Function to find a user by a key.
	public UserModel getUser() {
		UserController userController = new UserController();
		Scanner keyboard = new Scanner(System.in); // take input.

		System.out.print("Please enter search string: ");
		String key = keyboard.nextLine();

		return userController.find(key);
	}

	public String setAccountType() {
		Scanner keyboard = new Scanner(System.in);
		String accountType = "user"; // Set to user as default, for security.
		int getType = 0;

		while(getType == 0 || (getType < 1 || getType > 2)){
			System.out.println("Please select account type.");
			System.out.println("1) Admin");
			System.out.println("2) User");
			System.out.print("> ");

			while (!keyboard.hasNextInt()) {
				System.out.println("\n### ERROR ### ");
				System.out.println("### Please enter a number ### ");
				System.out.println("Please select account type.");
				System.out.println("1) Admin");
				System.out.println("2) User");
				System.out.print("> ");

				keyboard.nextLine();
			}
			getType = keyboard.nextInt();
		}

		// Select group.
		switch (getType){
			case 1:
				accountType = "administrator";
				break;
			case 2:
				accountType = "user";
				break;
		}

		return accountType;
	}

    public void removeUser() {
        int choice = 0;
        Scanner keyboard = new Scanner(System.in);
        UserController userController = new UserController();

        ArrayList<UserModel> users = userController.getAll();
        Map<Integer, UserModel> user = new HashMap<Integer, UserModel>();

        int n = 1;
        for(UserModel i : users){
            user.put(n, i);
            n++;
        }

        if(users != null){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            while(choice <= 0 || choice >n){
                for(int i =1; i < n; i++){
                    System.out.println(i+") "+user.get(i).getName());
                }
                System.out.println(n+") Exit");
                System.out.println("Please pick an user: ");
                System.out.print("> ");
                choice = keyboard.nextInt();

                if(choice != n){
                    users.remove(user.get(choice));
                    System.out.println(user.get(choice).getName()+" has been removed");
                }
            }
        }
    }
}