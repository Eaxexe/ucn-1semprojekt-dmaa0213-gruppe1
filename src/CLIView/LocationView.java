package CLIView;
import Model.*;
import Controller.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.ArrayList;

public class LocationView {

	public void menu() {
		Boolean exit = false;
		int choice;

		while(!exit){
			choice = menuChooser();

			switch(choice){
				case 1:
					createLocation();
					break;
				case 2:
					listLocations();
					break;
                case 3:
                    editLocation();
                    break;
                case 4:
                    removeLocation();
                    break;
				case 9:
					exit = true;
					break;
			}
		}
	}

	public int menuChooser() {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("- Locations -");
		System.out.println("1) Add location");
		System.out.println("2) Show all locations");
        System.out.println("3) Edit location");
        System.out.println("4) Remove location");
		System.out.println("9) Exit");
		System.out.print("> ");

		return keyboard.nextInt();
	}

	public void createLocation() {
		LocationController customerController = new LocationController();
		Scanner keyboard = new Scanner(System.in); // take input.
		String name;

		System.out.print("Please enter location name: ");
		name = keyboard.nextLine();

		// Add the location to the list.
		customerController.create(name);

		System.out.print("Location created.");
	}

	public void listLocations() {
		LocationController locationController = new LocationController();

		ArrayList<LocationModel> locations = locationController.list();
		if(locations != null ){
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

			for(LocationModel u : locations){
				System.out.println("X\tLocation name:\t"+u.getName());
				System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
			}
		}
	}

    public void editLocation() {
        int choice = 0;
        LocationController locationController = new LocationController();

        ArrayList<LocationModel> locations = locationController.list();
        Map<Integer, LocationModel> location = new HashMap<Integer, LocationModel>();

        int n = 1;
        for(LocationModel i : locations){
            location.put(n, i);
            n++;
        }

        if(locations != null ){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            for(LocationModel c : locations){
                Scanner keyboard = new Scanner(System.in);

                while(choice <= 0 || choice > n){
                    for(int i = 1; i < n; i++){
                        System.out.println(i+") "+location.get(i).getName());
                    }
                    System.out.println(n+") Exit");
                    System.out.println("Please pick a location: ");
                    System.out.print("> ");
                    choice = keyboard.nextInt();

                    if(choice != n){
                        System.out.println("Please enter the new name:");
                        c.setName(editField());
                        }
                    }
                }
        }
    }

    // Catch-all method to set the fields of a user being edited.
    public String editField() {
        System.out.print("> ");
        Scanner keyboard = new Scanner(System.in); // take input.
        return keyboard.nextLine();
    }

    public void removeLocation() {
        int choice = 0;
        Scanner keyboard = new Scanner(System.in);
        LocationController locationController = new LocationController();

        ArrayList<LocationModel> locations = locationController.list();
        Map<Integer, LocationModel> location = new HashMap<Integer, LocationModel>();

        int n = 1;
        for(LocationModel i : locations){
            location.put(n, i);
            n++;
        }

        if(locations != null ){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            while(choice <= 0 || choice > n){
                for(int i = 1; i < n; i++){
                    System.out.println(i+") "+location.get(i).getName());
                }
                System.out.println(n+") Exit");
                System.out.println("Please pick a location: ");
                System.out.print("> ");
                choice = keyboard.nextInt();

                if(choice != n){
                    locations.remove(location.get(choice));
                    System.out.println(location.get(choice).getName()+" has been removed");
                }
            }
        }
    }
}