package CLIView;
import Model.*;
import Controller.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GroupView {

    public void menu() {
        Boolean exit = false;
        int choice;

        while(!exit){
            choice = menuChooser();

            switch(choice){
                case 1:
                    createGroup();
                    break;
                case 2:
                    findGroup();
                    break;
                case 3:
                    listGroup();
                    break;
                case 4:
                    editGroup();
                    break;
                case 5:
                    removeGroup();
                    break;
                case 9:
                    exit = true;
                    break;
            }
        }
    }

    public int menuChooser() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("- ItemGroups -");
        System.out.println("1) Add Group");
        System.out.println("2) Find Group");
        System.out.println("3) List Groups");
        System.out.println("4) Edit Groups");
        System.out.println("5) Remove Groups");
        System.out.println("9) Exit");
        System.out.print("> ");

        return keyboard.nextInt();
    }

    public void createGroup() {
        GroupController groupController = new GroupController();
        Scanner keyboard = new Scanner(System.in); // take input.
        String name;
        String id;
        int antal;

        System.out.print("Please enter name: ");
        name = keyboard.nextLine();
        System.out.print("Please enter id: ");
        id = keyboard.nextLine();
        System.out.print("Please enter id: ");
        antal = keyboard.nextInt();
        groupController.create(name, id, antal);
    }

    public void findGroup() {
        GroupController groupController = new GroupController();
        Scanner keyboard = new Scanner(System.in); // take input.

        System.out.print("Please enter search string: ");
        String key = keyboard.nextLine();

        GroupModel g = groupController.find(key);

        if(g != null){
            System.out.println("Found a group by that info:");
            System.out.println("Name: "+g.getName());
            System.out.println("Id: "+g.getId());
        } else {
            System.out.println("Did not find group.");
        }
    }

    public void listGroup() {
        GroupController groupController = new GroupController();

        ArrayList<GroupModel> groups = groupController.getAll();
        if(groups.size() > 0){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            for(GroupModel u : groups){
                System.out.println("X\tName:\t\t"+u.getName());
                System.out.println("X\tId:\t\t\t"+u.getId());
                System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            }
        }
    }

    public void editGroup() {
        int choice = 0;
        Scanner keyboard = new Scanner(System.in);
        GroupController groupController = new GroupController();

        ArrayList<GroupModel> groups = groupController.getAll();
        Map<Integer, GroupModel> group = new HashMap<Integer, GroupModel>();

        int n = 1;
        for(GroupModel i : groups){
            group.put(n, i);
            n++;
        }

        if(groups.size() > 0){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            for(GroupModel c : groups){

                while(choice <= 0 || choice > n){
                    for(int i = 1; i < n; i++){
                        System.out.println(i+") "+group.get(i).getName());
                    }
                    System.out.println(n+") Exit");
                    System.out.println("Please pick a group: ");
                    System.out.print("> ");
                    choice = keyboard.nextInt();

                    if(choice != n){
                        System.out.println("Please enter the new name:");
                        c.setName(editField());
                        System.out.println("Please enter the new ID:");
                        c.setId(editField());
                    }
                }
            }
        }
    }

    // Catch-all method to set the fields of a user being edited.
    public String editField() {
        System.out.print("> ");
        Scanner keyboard = new Scanner(System.in); // take input.
        return keyboard.nextLine();
    }

    public void removeGroup() {
        int choice = 0;
        GroupController groupController = new GroupController();
        Scanner keyboard = new Scanner(System.in);

        ArrayList<GroupModel> groups = groupController.getAll();
        Map<Integer, GroupModel> group = new HashMap<Integer, GroupModel>();

        int n = 1;
        for(GroupModel i : groups){
            group.put(n, i);
            n++;
        }

        if(groups.size() > 0){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            while(choice <= 0 || choice > n){
                for(int i = 1; i < n; i++){
                    System.out.println(i+") "+group.get(i).getName());
                }
                System.out.println(n+") Exit");
                System.out.println("Please pick a group: ");
                System.out.print("> ");
               choice = keyboard.nextInt();

                if(choice != n){
                    groups.remove(group.get(choice));
                    System.out.println(group.get(choice).getName()+" has been removed");
                }
            }
        }
    }
}
