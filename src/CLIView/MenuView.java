package CLIView;
import java.util.Scanner;

public class MenuView {
	public void menu() {
		int choice;
		Boolean exit = false;

		while(!exit){
			choice = menuChooser();

			switch(choice){
				case 1:
					startCustomer();
					break;
				case 2:
					startItem();
					break;
                case 3:
                    startGroup();
                    break;
                case 4:
                    startUser();
                    break;
                case 5:
                    startDeal();
                    break;
				case 6:
					startLocation();
					break;
				case 7:
					startSales();
					break;
                case 8:
                    logout();
                    break;
				case 9:
					exit = true;
					break;
			}
		}
	}

	public int menuChooser() {
		Scanner keyboard = new Scanner(System.in);

		System.out.println("Sandy Ambien Nights:");
		System.out.println("1) Customer");
		System.out.println("2) Items");
        System.out.println("3) Item Groups");
        System.out.println("4) User");
        System.out.println("5) Deals");
		System.out.println("6) Locations");
		System.out.println("7) Sales");
        System.out.println("8) Logout");
		System.out.println("9) Exit");
		System.out.print("> ");

		return keyboard.nextInt();
	}

	public void startCustomer() {
		CustomerView customerView = new CustomerView();
		customerView.menu();
	}

	public void startItem() {
		ItemView itemView = new ItemView();
		itemView.menu();
	}

    public void startGroup() {
        GroupView groupView = new GroupView();
        groupView.menu();
    }

    public void startUser() {
        UserView userView = new UserView();
        userView.menu();
    }
    public void startDeal() {
        DealView dealView = new DealView();
        dealView.menu();
    }

	public void startSales() {
		SalesView salesView = new SalesView();
		salesView.menu();
	}

	public void startLocation() {
		LocationView locationView = new LocationView();
		locationView.menu();
	}

    public void logout() {
        LoginView loginView = new LoginView();
        loginView.login();
    }
}

